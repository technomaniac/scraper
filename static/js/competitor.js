

function get_competitor_chart(data) {
    $('#competitor_assortment').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: data.level + ' Level Discount Assortment'
        },
        xAxis: {
            categories: [
                'Total Products',
                '0-10 %',
                '10-20 %',
                '20-30 %',
                '30-40 %',
                '40-50 %',
                '50-60 %',
                '60-70 %',
                '70-80 %',
                '80-90 %',
                '90-100 %'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Products'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"> <b> {point.y:1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0,
                borderWidth: 0
            }
        },
        series: data.data

    });

}

function get_category_chart(data) {
    $('#category_assortment').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top Category Level Assortment'
        },
        xAxis: {
            categories: data.competitors,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Products'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"> <b> {point.y:1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0,
                borderWidth: 0
            }
        },
        series: data.data

    });

}

function get_competitor_chart_data() {
    //var params = getUrlVars();
    $.ajax({
        url: '/analytics/chart/competitor/',
        //data: params,
        contentType: "application/json",
        dataType: 'json',
        success: function (response) {
            get_competitor_chart(response);
        }
    });
}

function get_category_chart_data() {
    //var params = getUrlVars();
    $.ajax({
        url: '/analytics/chart/category/',
        //data: params,
        contentType: "application/json",
        dataType: 'json',
        success: function (response) {
            get_category_chart(response);
        }
    });
}

$(document).ready(function () {
    get_competitor_chart_data();
    get_category_chart_data();
});

setTimeout(function () {
    get_competitor_chart_data();
    get_category_chart_data();
}, 30 * 60 * 1000);

