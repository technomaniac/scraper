function get_goals_conversion_funnel_chart(data, element) {
    $(element).highcharts({
        chart: {
            type: 'funnel',
            marginRight: 100
        },
        title: {
            text: data[0].name,
            x: -50
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b> ({point.y:,.0f})',
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                    softConnector: true
                },
                neckWidth: '30%',
                neckHeight: '25%'

                //-- Other available options
                // height: pixels or percent
                // width: pixels or percent
            }
        },
        legend: {
            enabled: false
        },
        series: data
    });
}


$(document).ready(function () {
    $.ajax({
        url: '/analytics/chart/goals/',
        contentType: "application/json",
        dataType: 'json',
        success: function (response) {
            get_goals_conversion_funnel_chart(response.overall, '#goals_conversion_funnel_overall');
            get_goals_conversion_funnel_chart(response.india, '#goals_conversion_funnel_india');
            get_goals_conversion_funnel_chart(response.row, '#goals_conversion_funnel_row');
        }
    });
});