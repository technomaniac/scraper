import debug_toolbar
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = patterns('',
                       url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
                           {'document_root': settings.STATIC_ROOT}),
                       url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
                           {'document_root': settings.MEDIA_ROOT}),
                       url(r'^__debug__/', include(debug_toolbar.urls)),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^analytics/', include('apps.analytics.urls')),
                       )
