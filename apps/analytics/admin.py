from django.contrib import admin

from .models import *

# Register your models here.
admin.site.register(Goals)
admin.site.register(GoalCompletion)

admin.site.register(GoalCompletionByCountryBrowser)
admin.site.register(GoalConversionRatioHistory)