from datetime import datetime
import csv
import StringIO

from django.core.mail.message import EmailMessage
from django.db.models.aggregates import Count, Sum, Avg
from django.db.models.expressions import When, Case
from django.db.models.fields import IntegerField

from django.db import connection

from apps.catalogue.models import Product
from scraper.settings import EMAIL_HOST_USER


class DiscountAssortment(object):
    order_by = ['-total_products']
    # sub_cat_level_values = ['sub_category__name', 'brand__name', 'competitor__name']
    # brand_level_values = ['brand__name', 'competitor__name']
    # competitor_level_values = ['competitor__name']

    SUB_CAT_VIEW = 'sub_cat_level_dataset'
    BRAND_VIEW = 'brand_level_dataset'
    COMPETITOR_VIEW = 'competitor_level_dataset'

    SUB_CAT_VIEW_FIELDS = ['competitor_id', 'competitor_name', 'brand_id', 'brand_name', 'sub_cat_id', 'sub_cat_name',
                           'total_products', 'avg_discount', 'disc_0_10', 'disc_10_20', 'disc_20_30', 'disc_30_40',
                           'disc_40_50', 'disc_50_60', 'disc_60_70', 'disc_70_80', 'disc_80_90', 'disc_90_100']

    BRAND_VIEW_FIELDS = ['competitor_id', 'competitor_name', 'brand_id', 'brand_name',
                         'total_products', 'avg_discount', 'disc_0_10', 'disc_10_20', 'disc_20_30', 'disc_30_40',
                         'disc_40_50', 'disc_50_60', 'disc_60_70', 'disc_70_80', 'disc_80_90', 'disc_90_100']

    COMPETITOR_VIEW_FIELDS = ['competitor_id', 'competitor_name',
                              'total_products', 'avg_discount', 'disc_0_10', 'disc_10_20', 'disc_20_30', 'disc_30_40',
                              'disc_40_50', 'disc_50_60', 'disc_60_70', 'disc_70_80', 'disc_80_90', 'disc_90_100']

    def __init__(self, level='sub-cat', page_size=None, page=None, order_by=None, **filter):
        self.cursor = connection.cursor()
        self.data = []
        self.count = 0
        self.sub_cat_level_data = list()
        self.brand_level_data = list()
        self.competitor_level_data = list()
        self.level = level
        self.page = page
        self.total_pages = 0

        if level == 'brand':
            self.view = self.BRAND_VIEW
            self.fields = self.BRAND_VIEW_FIELDS
        elif level == 'competitor':
            self.view = self.COMPETITOR_VIEW
            self.fields = self.COMPETITOR_VIEW_FIELDS
        else:
            self.view = self.SUB_CAT_VIEW
            self.fields = self.SUB_CAT_VIEW_FIELDS

        self.cursor.execute('''select count(*) from {}'''.format(self.view))
        self.total_results = self.cursor.fetchone()[0]

        if self.total_results > 0:
            self.sql = '''select * from {}'''.format(self.view)

            if filter:
                where = ' where'
                where += ' and'.join([' {} in ({})'.format(key, ','.join(value)) for key, value in filter.iteritems()])

                self.sql = self.sql + where

            if order_by:
                if order_by[0] == '-':
                    order_by = ' order by {} desc'.format(order_by[1:])
                else:
                    order_by = ' order by {}'.format(order_by)

                self.sql = self.sql + order_by

            if page_size:
                limit = ' limit {}'.format(page_size)
                self.sql = self.sql + limit
            else:
                page_size = self.total_results

            if page and page_size:
                self.page = int(page)
                self.total_pages = self.total_results / int(page_size)
                if self.total_results % int(page_size) > 0:
                    self.total_pages += 1

                offset = ' offset {}'.format(int(page_size) * (int(page) - 1))
                self.sql = self.sql + offset
            else:
                self.page = 1
                self.total_pages = self.total_results / int(page_size)
                if self.total_results % int(page_size) > 0:
                    self.total_pages += 1

            self.data = self.get_data()
        else:
            self.data = []

    def get_data(self):
        self.cursor = connection.cursor()
        self.cursor.execute(self.sql)
        self.query_result = self.cursor.fetchall()
        return self.convert_to_dict(self.query_result, self.fields)

    def create_csv(self):
        filename = '{}_LEVEL_ASSORTMENT_DATA_{}.csv'.format(self.level.upper(),
                                                            datetime.now().strftime('%d-%M-%Y_%H-%M'))
        # directory = MEDIA_ROOT + 'assortment/{}_level/'.format(self.level)

        csv_file = StringIO.StringIO()
        wr = csv.writer(csv_file, quoting=csv.QUOTE_ALL)
        header, fields = self.get_csv_header_field()
        wr.writerow(header)

        for row in self.get_data_row(fields):
            wr.writerow(row)

        return filename, csv_file

    def send_csv(self, emails=[]):
        filename, csv_file = self.create_csv()

        message = EmailMessage(subject='{} LEVEL ASSORTMENT REPORT'.format(self.level.upper()),
                               from_email=EMAIL_HOST_USER, to=emails)
        message.attach(filename, csv_file.getvalue(), 'text/csv')
        print message.send()

    def get_data_row(self, fields):
        for row in self.data:
            yield [unicode(row[i]).encode('utf-8') for i in fields]

    def get_csv_header_field(self):
        COMMON_HEADER = ['Total Products', 'Average Discount %', 'Discount (0-10%)', 'Discount (10-20%)',
                         'Discount (20-30%)', 'Discount (30-40%)', 'Discount (40-50%)', 'Discount (50-60%)',
                         'Discount (60-70%)', 'Discount (70-80%)', 'Discount (80-90%)', 'Discount (90-100%)']

        COMPETITOR_HEADER = ['Competitor Name'] + COMMON_HEADER
        BRAND_HEADER = ['Competitor Name', 'Brand Name'] + COMMON_HEADER
        SUB_CATEGORY_HEADER = ['Competitor Name', 'Brand Name', 'Sub-Category'] + COMMON_HEADER
        non_csv_fields = ['competitor_id', 'brand_id', 'sub_cat_id']

        if self.level == 'brand':
            return BRAND_HEADER, [i for i in self.BRAND_VIEW_FIELDS if
                                  i not in non_csv_fields]
        if self.level == 'competitor':
            return COMPETITOR_HEADER, [i for i in self.COMPETITOR_VIEW_FIELDS if
                                       i not in non_csv_fields]
        else:
            return SUB_CATEGORY_HEADER, [i for i in self.SUB_CAT_VIEW_FIELDS if
                                         i not in non_csv_fields]

    # def get_sub_cat_level_data(self, sql, fields):
    #     self.cursor.execute(self.SUB_CAT_LEVEL_SQL)
    #     self.data, self.count = self.convert_to_dict(self.cursor.fetchall(), self.SUB_CAT_VIEW_FIELDS)
    #     return self.data
    #
    # def get_brand_level_data(self):
    #     self.cursor.execute(self.BRAND_LEVEL_SQL)
    #     self.data, self.count = self.convert_to_dict(self.cursor.fetchall(), self.BRAND_VIEW_FIELDS)
    #     return self.data
    #
    # def get_competitor_level_data(self):
    #     self.cursor.execute(self.COMPETITOR_LEVEL_SQL)
    #     self.data, self.count = self.convert_to_dict(self.cursor.fetchall(), self.COMPETITOR_VIEW_FIELDS)
    #     return self.data

    @staticmethod
    def convert_to_dict(row_object, fields):
        # count = 0
        result = list()
        for row in row_object:
            row_dict = dict()
            for i in xrange(len(row)):
                row_dict[fields[i]] = row[i]
            result.append(row_dict)
            # count += 1
        return result

    # self.initialize_query()
    #
    # def initialize_query(self):
    #     self.set_brand_level_data()
    #     self.set_competitor_level_data()
    #     self.set_sub_cat_level_data()

    # def get_sub_cat_level_data(self):
    #     if cache.get('sub_cat_level_data'):
    #         self.sub_cat_level_data = cache.get('sub_cat_level_data')
    #     else:
    #         self.sub_cat_level_data = self.get_query_result(self.sub_cat_level_values)
    #         self.set_cache(self.sub_cat_level_data, 'sub_cat_level_data')
    #
    #     return self.sub_cat_level_data
    #
    # def get_brand_level_data(self):
    #     if self.get_cache('brand_level_data'):
    #         self.brand_level_data = self.get_cache('brand_level_data')
    #     else:
    #         self.brand_level_data = self.get_query_result(self.brand_level_values)
    #         self.set_cache(self.brand_level_data, 'brand_level_data')
    #
    #     return self.brand_level_data
    #
    # def get_competitor_level_data(self):
    #     if self.get_cache('competitor_level_data'):
    #         self.competitor_level_data = self.get_cache('competitor_level_data')
    #     else:
    #         self.competitor_level_data = self.get_query_result(self.competitor_level_values)
    #         self.set_cache(self.competitor_level_data, 'competitor_level_data')
    #
    #     return self.competitor_level_data
    #
    # @staticmethod
    # def set_cache(queryset, key):
    #     cache.set(queryset, key)
    #
    # @staticmethod
    # def get_cache(key):
    #     if cache.get(key):
    #         return cache.get(key)
    #     else:
    #         return None
    #
    # @staticmethod
    # def delete_cache(key):
    #     cache.delete(key)

    def get_query_result(self, values=()):
        return Product.objects.all().values(*values) \
            .annotate(total_products=Count('id'), avg_discount=Avg('discount'),
                      disc_0_10=Sum(Case(When(discount__range=(0, 10), then=1)), default=0,
                                    output_field=IntegerField()),
                      disc_10_20=Sum(Case(When(discount__gt=10, discount__lte=20, then=1)), default=0,
                                     output_field=IntegerField()),
                      disc_20_30=Sum(Case(When(discount__gt=20, discount__lte=30, then=1)), default=0,
                                     output_field=IntegerField()),
                      disc_30_40=Sum(Case(When(discount__gt=30, discount__lte=40, then=1)), default=0,
                                     output_field=IntegerField()),
                      disc_40_50=Sum(Case(When(discount__gt=40, discount__lte=50, then=1)), default=0,
                                     output_field=IntegerField()),
                      disc_50_60=Sum(Case(When(discount__gt=50, discount__lte=60, then=1)), default=0,
                                     output_field=IntegerField()),
                      disc_60_70=Sum(Case(When(discount__gt=60, discount__lte=70, then=1)), default=0,
                                     output_field=IntegerField()),
                      disc_70_80=Sum(Case(When(discount__gt=70, discount__lte=80, then=1)), default=0,
                                     output_field=IntegerField()),
                      disc_80_90=Sum(Case(When(discount__gt=80, discount__lte=90, then=1)), default=0,
                                     output_field=IntegerField()),
                      disc_90_100=Sum(Case(When(discount__gt=90, discount__lte=100, then=1)), default=0,
                                      output_field=IntegerField())) \
            .order_by(*self.order_by)
