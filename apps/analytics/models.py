from datetime import datetime, timedelta

from django.db import models, connection
from django.db.models.aggregates import Sum
from django.db.models.query_utils import Q
from django_pgjson.fields import JsonBField

from .ga.constants import AVG_GOAL_COMPLETION_SQL



class Goals(models.Model):
    goal_id = models.IntegerField(unique=True)
    goal_name = models.CharField(null=True, blank=True, max_length=50)
    display_name = models.CharField(null=True, blank=True, max_length=100)
    predecessor = models.ForeignKey('self', null=True, blank=True)
    overall_conversion_ratio = models.FloatField(null=True, blank=True)
    india_conversion_ratio = models.FloatField(null=True, blank=True)
    row_conversion_ratio = models.FloatField(null=True, blank=True)
    overall_average_completions = models.FloatField(null=True, blank=True)
    india_average_completions = models.FloatField(null=True, blank=True)
    row_average_completions = models.FloatField(null=True, blank=True)
    chrome_conversion_ratio = models.FloatField(null=True, blank=True)
    ie_conversion_ratio = models.FloatField(null=True, blank=True)
    safari_conversion_ratio = models.FloatField(null=True, blank=True)
    other_browser_conversion_ratio = models.FloatField(null=True, blank=True)

    def __unicode__(self):
        return '{}, Ratio : {}'.format(self.goal_name.encode('utf-8'), self.overall_conversion_ratio)

    @classmethod
    def get_avg_goal_completions(cls, current_hour=False):
        date = datetime.now()
        where = "where (gc.date between now()::date -8 and now()::date -1 and gc.hour={})".format(
            int(date.strftime('%H')))
        if current_hour:
            where = "where (gc.date='{}' and gc.hour={})".format(date.strftime('%Y-%m-%d'), int(date.strftime('%H')))
        where += " and (cb.source not ilike '%payu%' or cb.source is null)"
        cursor = connection.cursor()
        cursor.execute(AVG_GOAL_COMPLETION_SQL.format(where))
        # print AVG_GOAL_COMPLETION_SQL.format(where)
        rows = cursor.fetchall()

        result = []
        for i in xrange(len(rows)):
            if i == 0:
                result.append(GoalAvgCompletions(*rows[i]))
            else:
                result.append(GoalAvgCompletions(*rows[i], predecessor=result[i - 1]))
        return result

    def get_overall_conversion_percentage(self):
        predecessor_average_completion = self.predecessor.overall_average_completions
        diff = (predecessor_average_completion - self.overall_average_completions)
        return round((diff / predecessor_average_completion) * 100, 2)

    def get_india_conversion_percentage(self):
        predecessor_average_completion = self.predecessor.india_average_completions
        diff = (predecessor_average_completion - self.india_average_completions)
        return round((diff / predecessor_average_completion) * 100, 2)

    def get_row_conversion_percentage(self):
        predecessor_average_completion = self.predecessor.row_average_completions
        diff = (predecessor_average_completion - self.row_average_completions)
        return round((diff / predecessor_average_completion) * 100, 2)

    # def get_avg_goal_completion(self, days=7, current_hour=False):
    #     current_date = datetime.now()
    #     filter = dict()
    #     if current_hour:
    #         filter['hour'] = int(current_date.strftime('%H'))
    #         filter['date'] = current_date
    #     else:
    #         filter['date__gte'] = current_date - timedelta(days=days)
    #     filter['goal'] = self
    #
    #     query = GoalCompletion.objects.filter(**filter).values('goal_id').annotate(
    #         average_completion=Avg('completions'))
    #     return query[0]['average_completion']
    #
    # def get_avg_india_goal_completion(self, days=7, current_hour=False):
    #     current_date = datetime.now()
    #     filter = dict()
    #     if current_hour:
    #         filter['hour'] = int(current_date.strftime('%H'))
    #         filter['date'] = current_date
    #     else:
    #         filter['date__gte'] = current_date - timedelta(days=days)
    #     filter['goal__pk'] = self.pk
    #     filter['goal_completion_by_country__country'] = 'India'
    #
    #     query = GoalCompletion.objects.filter(**filter).values('goal_id', ).annotate(
    #         average_completion=Sum('goal_completion_by_country__completions'))
    #     return query[0]['average_completion']
    #
    # def get_avg_row_goal_completion(self, days=7, current_hour=False):
    #     current_date = datetime.now()
    #     filter = dict()
    #     if current_hour:
    #         filter['hour'] = int(current_date.strftime('%H'))
    #         filter['date'] = current_date
    #     else:
    #         filter['date__gte'] = current_date - timedelta(days=days)
    #     filter['goal__pk'] = self.pk
    #
    #     query = GoalCompletion.objects.filter(**filter).exclude(goal_completion_by_country__country='Indian').values(
    #         'goal_id').annotate(average_completion=Sum('goal_completion_by_country__completions'))
    #     return query[0]['average_completion']
    #
    @classmethod
    def get_goals_completion_count(cls, days=7, current_hour=False, region='overall'):
        current_date = datetime.now()
        filter = dict()
        if current_hour:
            filter['goal_completion__hour'] = int(current_date.strftime('%H'))
            filter['goal_completion__date'] = current_date
        else:
            filter['goal_completion__date__gte'] = current_date - timedelta(days=days)

        if region == 'India':
            filter['country'] = 'India'

        if region == 'ROW':
            return GoalCompletionByCountryBrowser.objects.filter(**filter).exclude(country='India').values(
                'goal_completion__goal__display_name').annotate(total_completions=Sum('completions')).order_by(
                '-total_completions')

        return GoalCompletionByCountryBrowser.objects.filter(**filter).values(
            'goal_completion__goal__display_name').annotate(total_completions=Sum('completions')).order_by(
            '-total_completions')

class GoalConversionRatioHistory(models.Model):
    goal = models.ForeignKey(Goals, related_name='goal_conversion_history')
    overall_conversion_ratio = models.FloatField(null=True, blank=True)
    current_overall_conversion_ratio = models.FloatField(null=True, blank=True)
    ocr_diff = models.FloatField(null=True, blank=True)
    india_conversion_ratio = models.FloatField(null=True, blank=True)
    current_india_conversion_ratio = models.FloatField(null=True, blank=True)
    icr_diff = models.FloatField(null=True, blank=True)
    row_conversion_ratio = models.FloatField(null=True, blank=True)
    current_row_conversion_ratio = models.FloatField(null=True, blank=True)
    rcr_diff = models.FloatField(null=True, blank=True)
    chrome_conversion_ratio = models.FloatField(null=True, blank=True)
    current_chrome_conversion_ratio = models.FloatField(null=True, blank=True)
    ie_conversion_ratio = models.FloatField(null=True, blank=True)
    current_ie_conversion_ratio = models.FloatField(null=True, blank=True)
    safari_conversion_ratio = models.FloatField(null=True, blank=True)
    current_safari_conversion_ratio = models.FloatField(null=True, blank=True)
    other_browser_conversion_ratio = models.FloatField(null=True, blank=True)
    current_other_browser_conversion_ratio = models.FloatField(null=True, blank=True)
    created_on = models.DateTimeField(default=datetime.now())
    alerted = JsonBField(null=True, blank=True)

class GoalCompletion(models.Model):
    goal = models.ForeignKey(Goals, related_name='goal_completion')
    date = models.DateField()
    hour = models.IntegerField()
    completions = models.IntegerField()


    def __unicode__(self):
        return 'GoalId: {}, Date: {}, Hour: {}, Completions: {}'.format(self.goal.pk, self.date, self.hour,
                                                                        self.completions)

    class Meta:
        unique_together = ('goal', 'date', 'hour')


# class GoalCompletionByCountryBrowserQueryset(QuerySet):
#     def objects(self, *args, **kwargs):
#         return self.filter(~Q(source__icontains='payu'))
#
#     # def all(self, *args, **kwargs):
#     #     return self.filter(~Q(source__icontains='payu'))


class GoalCompletionByCountryBrowserManager(models.Manager):
    def get_queryset(self):
        return super(GoalCompletionByCountryBrowserManager, self).get_queryset().filter(~Q(source__icontains='payu'))

class GoalCompletionByCountryBrowser(models.Model):
    goal_completion = models.ForeignKey(GoalCompletion, related_name='goal_completion_by_country')
    country = models.CharField(null=True, blank=True, max_length=50)
    browser = models.CharField(null=True, blank=True, max_length=50)
    source = models.CharField(null=True, blank=True, max_length=200)
    completions = models.IntegerField()

    objects = GoalCompletionByCountryBrowserManager()

    class Meta:
        unique_together = ('goal_completion', 'country', 'browser', 'source')

    def __unicode__(self):
        return 'GoalId: {}, Browser: {}, Source: {}, Completions: {}'.format(self.goal_completion.goal.pk,
                                                                     #self.country.encode('utf-8'),
                                                                     self.browser,
                                                                             self.source,
                                                                     self.completions)


class GoalAvgCompletions(object):
    def __init__(self, id, goal, overall_goal_completions, india_goal_completions, row_goal_completions,
                 chrome_completions, ie_completions, safari_completions, other_browser_completions,
                 predecessor=None):
        self.id = id
        self.goal = goal
        self.overall_goal_completions = overall_goal_completions
        self.india_goal_completions = india_goal_completions
        self.row_goal_completions = row_goal_completions

        self.overall_goal_percent = ''
        self.india_goal_percent = ''
        self.row_goal_percent = ''

        self.chrome_completions = chrome_completions
        self.ie_completions = ie_completions
        self.safari_completions = safari_completions
        self.other_browser_completions = other_browser_completions

        self.chrome_conversion_ratio = 0
        self.safari_conversion_ratio = 0
        self.ie_conversion_ratio = 0
        self.other_browser_conversion_ratio = 0

        if predecessor:
            if predecessor.overall_goal_completions != 0:
                self.overall_goal_percent = round((float(self.overall_goal_completions) / float(
                    predecessor.overall_goal_completions)) * 100, 2)

            if predecessor.india_goal_completions != 0:
                self.india_goal_percent = round((float(self.india_goal_completions) / float(
                    predecessor.india_goal_completions)) * 100, 2)

            if predecessor.row_goal_completions != 0:
                self.row_goal_percent = round((float(self.row_goal_completions) / float(
                    predecessor.row_goal_completions)) * 100, 2)

            if predecessor.chrome_completions != 0:
                self.chrome_conversion_ratio = round(
                    (float(self.chrome_completions) / float(predecessor.chrome_completions)), 2)

            if predecessor.safari_completions != 0:
                self.safari_conversion_ratio = round(
                    (float(self.safari_completions) / float(predecessor.safari_completions)), 2)

            if predecessor.ie_completions != 0:
                self.ie_conversion_ratio = round(
                    (float(self.ie_completions) / float(predecessor.ie_completions)), 2)

            if predecessor.other_browser_completions != 0:
                self.other_browser_conversion_ratio = round(
                    (float(self.other_browser_completions) / float(predecessor.other_browser_completions)), 2)
