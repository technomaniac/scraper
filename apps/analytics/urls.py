from django.conf.urls import patterns, url

from .views import *

urlpatterns = patterns('apps.analytics.views',
                       url(r'^assortment/$', AssortmentView.as_view()),
                       url(r'^chart/competitor/$', get_competitor_chart_data),
                       url(r'^chart/category/$', get_category_chart_data),
                       url(r'^chart/goals/$', get_goal_conversion_funnel_data),
                       url(r'^goals/$', get_goal_conversion_funnel_dashboard),
                       )
