table_id = 'ga:53703868'
base_num_users = 350
goals_conversion_metrics = 'ga:goalCompletionsAll,ga:goal9Completions,ga:goal10Completions,ga:goal4Completions,ga:goal11Completions,ga:goal1Completions'
username = 'reporting@exclusively.com'
password = 'snapdeal@lakhan'
smtp_server = 'smtp.gmail.com:587'
# mailing_list = ['abhishek.verma03@exclusively.com']
mailing_list = ['abhishek.verma03@exclusively.com',
                'analytics@exclusively.com'
                'pmo@exclusively.com',
                'amit@exclusively.com',
                'abhishek.passi@exclusively.com',
                'raj@exclusively.com',
                'badrinath.mishra@exclusively.com',
                'amrish.garg@exclusively.com',
                'Jayant.yadav@exclusively.com',
                'arjun@exclusively.in']

goals_mailing_list = ["abhishek.verma03@exclusively.com",
                      "analytics@exclusively.com",
                      "pmo@exclusively.com",
                      "amit@exclusively.com",
                      "abhishek.passi@exclusively.com",
                      "raj@exclusively.com",
                      "badrinath.mishra@exclusively.com",
                      "amrish.garg@exclusively.com",
                      "Jayant.yadav@exclusively.com",
                      "arjun@exclusively.in",
                      "chauhan.vivek@exclusively.com",
                      "aradhana.singh@exclusively.com",
                      "chandan.deka@exclusively.com",
                      "varun.jha@exclusively.com",
                      "raj@exclusively.com",
                      "brajesh.rawat@exclusively.com",
                      "animesh@exclusively.com"]

GA_DATE_FORMAT = '%Y-%m-%d'

GA_GOALS = {
    'goal10Completions': {'predecessor': 'goal9Completions', 'order': 2, 'id': 10, 'name': 'Viewed Product Page'},
    'goal11Completions': {'predecessor': 'goal4Completions', 'order': 4, 'id': 11, 'name': 'Visit OnePageCheckout'},
    'goal1Completions': {'predecessor': 'goal11Completions', 'order': 5, 'id': 1, 'name': 'Successful Checkout'},
    'goal4Completions': {'predecessor': 'goal10Completions', 'order': 3, 'id': 4, 'name': 'Visit Cart Page'},
    'goal9Completions': {'predecessor': 'goalCompletionsAll', 'order': 1, 'id': 9, 'name': 'Viewed Category Page'},
    'goalCompletionsAll': {'predecessor': None, 'order': 0, 'id': 0, 'name': 'Total Visits'}
}

CRITICAL_DROP = 10
GOALS_NUM_DAYS = 7

GA_GOALS_ORDER = [u'ga:goalCompletionsAll',
                  u'ga:goal9Completions',
                  u'ga:goal10Completions',
                  u'ga:goal4Completions',
                  u'ga:goal11Completions',
                  u'ga:goal1Completions'],

AVG_GOAL_COMPLETION_SQL = '''select id,goal,avg(completions) as avg_completions,avg(india_completions) as avg_india_completions,
                                avg(row_completions) as avg_row_completions,
                                avg(chrome_completions) as chrome_completions,
                                avg(ie_completions) as ie_completions,
                                avg(safari_completions) as safari_completions,
                                avg(other_browser_completions) as other_browser_completions from
                                (select g.id as id, g.display_name as goal, gc.date, gc.hour,
                                    sum(cb.completions) as completions,
                                    sum(case when cb.country='India' then cb.completions end) as india_completions,
                                    sum(case when cb.country!='India' then cb.completions end) as row_completions,
                                    sum(case when cb.browser='Chrome' then cb.completions end) as chrome_completions,
                                    sum(case when cb.browser='Internet Explorer' then cb.completions end) as ie_completions,
                                    sum(case when cb.browser='Safari' then cb.completions end) as safari_completions,
                                    sum(case when cb.browser not in ('Safari','Internet Explorer','Chrome') then cb.completions end) as other_browser_completions
                                    from analytics_goalcompletion as gc
                                    join analytics_goals g on g.id = gc.goal_id
                                    join analytics_goalcompletionbycountrybrowser cb on gc.id = cb.goal_completion_id
                                    {}
                                    group by g.id,g.display_name,gc.date,gc.hour)
                                as avg_comp
                            group by id,goal
                            order by avg_completions desc'''
