import argparse
import sys
import time
import smtplib
import os
import json
from datetime import datetime

import redis
from googleapiclient import sample_tools

from constants import *

argparser = argparse.ArgumentParser(add_help=False)
argparser.add_argument('table_id', type=str,
                       help=('The table ID of the profile you wish to access. '
                             'Format is ga:xxx where xxx is your profile ID.'))

r = redis.StrictRedis(host='localhost', port=6379, db=1)


def main(argv):
    service, flags = initialize_ga_service(argv=argv, parents=[argparser])
    results = get_real_time_data(service, flags.table_id)
    update_data(results)

    # service, flags = initialize_ga_service()
    # results = get_goals_conversion_data(service, table_id)
    # pprint.pprint(results)


def initialize_ga_service(argv=None, parents=()):
    if not argv:
        argv = [table_id]

    return sample_tools.init(argv, 'analytics', 'v3', __doc__, __file__, parents=parents,
                             scope='https://www.googleapis.com/auth/analytics.readonly')

def get_real_time_data(service, table_id):
    return service.data().realtime().get(
        ids=table_id,
        metrics='rt:activeUsers', dimensions='rt:source').execute()


def get_goals_conversion_data(service, table_id, metrics=None, start_date=None, end_date=None, dimensions=None):
    if not start_date and not end_date:
        date = datetime.now().strftime(GA_DATE_FORMAT)
        start_date = date
        end_date = date
    if not dimensions:
        dimensions = 'ga:country,ga:browser,ga:source'
    if not metrics:
        metrics = goals_conversion_metrics
    return service.data().ga().get(ids=table_id, metrics=metrics, start_date=start_date, end_date=end_date,
                                   dimensions=dimensions).execute()



def send_alert(results, alert_list=None):
    active_users = int(results['totalsForAllResults']['rt:activeUsers'])
    if active_users < 150:
        bucket = 0
    else:
        bucket = ((active_users - 150) / 100) + 1

    if not r.get('bucket'):
        r.set('bucket', bucket)

    old_bucket = int(r.get('bucket'))

    timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    subject = '***GA Alert***'
    text = ''
    if abs(old_bucket - bucket) > 1:
        text += 'Current Active Users : {}'.format(active_users)
        r.set('bucket', bucket)

    if alert_list:
        text += '\n'
        for i in alert_list:
            text += '\n{} -\n'.format(i[0])
            text += '    Average: {}\n'.format(i[1]['avg'])
            text += '    Users: {}\n'.format(i[1]['queue'][-1])

    if text != '':
        send_email(mailing_list, subject, text)
        print('{}: Mail Status- Sent, Active Users- {}, Bucket- {}'.format(timestamp, active_users, bucket))
    else:
        print('{}: Mail Status- Not Sent, Active Users- {}, Bucket- {}'.format(timestamp, active_users, bucket))


def send_email(to, subject, msg):
    message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (username, ", ".join(to), subject, msg)

    server = smtplib.SMTP('smtp.gmail.com:587')
    server.starttls()
    server.login(username, password)
    server.sendmail(username, to, message)
    server.quit()


def update_data(results):
    timestamp = int(time.time())
    QUEUE_SIZE = 7 * 86400
    PERCENT = 0.2
    alert_list = list()
    with open('ga_data.json', 'r+') as f:
        if os.path.getsize(f.name) == 0:
            data = dict()
            data['total_hits'] = 0
        else:
            data = json.load(f)

        if data['total_hits'] < QUEUE_SIZE:
            data['total_hits'] = data['total_hits'] + 1

            for row in results['rows']:
                if row[0] not in data:
                    data[row[0]] = dict()

                    data[row[0]]['queue'] = [int(row[1])]
                    data[row[0]]['sum'] = int(row[1])
                    data[row[0]]['avg'] = float(data[row[0]]['sum']) / data['total_hits']
                    data[row[0]]['mail_status'] = {'timestamp': timestamp, 'status': False}
                else:
                    data[row[0]]['queue'].append(int(row[1]))
                    data[row[0]]['sum'] = data[row[0]]['sum'] + int(row[1])
                    data[row[0]]['avg'] = float(data[row[0]]['sum']) / data['total_hits']
                    if data[row[0]]['mail_status']['timestamp'] - timestamp > 3600:
                        data[row[0]]['mail_status'] = {'timestamp': timestamp, 'status': False}
                        # delta = abs(data[row[0]]['avg'] - int(row[1]))
                        # print delta
                        # if delta >= float(data[row[0]]['avg']) * (PER_DELTA / 100):
                        #     alert_list.append((row[0], data[row[0]]))
        else:
            for row in results['rows']:
                if row[0] not in data:
                    data[row[0]] = dict()
                    data[row[0]]['queue'] = [int(row[1])]
                    data[row[0]]['sum'] = int(row[1])
                    data[row[0]]['avg'] = float(data[row[0]]['sum']) / data['total_hits']
                    data[row[0]]['mail_status'] = {'timestamp': timestamp, 'status': False}
                else:
                    first = data[row[0]]['queue'].pop(0)
                    data[row[0]]['queue'].append(int(row[1]))
                    data[row[0]]['sum'] = data[row[0]]['sum'] - first + int(row[1])

                    if data[row[0]]['mail_status']['timestamp'] - timestamp > 3600:
                        data[row[0]]['mail_status'] = {'timestamp': timestamp, 'status': False}

                    if data[row[0]]['mail_status']['status'] == False:
                        delta = abs(data[row[0]]['avg'] - int(row[1]))

                        if delta > data[row[0]]['avg'] * PERCENT:
                            data[row[0]]['mail_status'] = {'timestamp': timestamp, 'status': True}
                            alert_list.append((row[0], data[row[0]]))

                    data[row[0]]['avg'] = float(data[row[0]]['sum']) / data['total_hits']

        send_alert(results, alert_list=alert_list)
        # r.set('ga_data', json.dumps(data))
        f.seek(0)
        f.write(json.dumps(data))
        f.truncate()


# def send_message_tracking():
#     return requests.post(
#         "https://api.mailgun.net/v3/sandboxccc752c35a3748ea93c86c68669fb0ea.mailgun.org/messages",
#         auth=("api", "key-2e1a565eabf7baf8c5f9fd28ed95541e"),
#         data={"from": "Excited User <mailgun@sandboxccc752c35a3748ea93c86c68669fb0ea.mailgun.org>",
#               "to": ["abhishek.verma03@exclusively.com", "vivek.singh01@exclusively.com"],
#               "subject": "Hello",
#               "html": "<html>It is so simple to send a message.<br/>Right?</html>",
#               "o:tracking": True,
#               "o:tracking-clicks": 'yes',
#               "o:tracking-opens": 'yes',
#               'v:my-custom-data': {"message_id": "%recipient.id%"},
#               "recipient-variables": ('{"abhishek.verma03@exclusively.com": {"first":"Abhishek", "id":1}, '
#                                       '"vivek.singh01@exclusively.com": {"first":"Vivek", "id":2}}')},
#         verify=False)


if __name__ == '__main__':
    try:
        main(sys.argv)
    # send_message_tracking()
    except Exception as e:
        print e
