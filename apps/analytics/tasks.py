import subprocess
import os

from celery.task import task

from django.core.mail.message import EmailMessage

from django.core.mail import EmailMultiAlternatives

from django.template.loader import get_template

from django.template import Context

from ..base.utils import extract_numbers
from .models import *
from ..scraper.tasks import refresh_mat_views
from scraper.settings import EMAIL_HOST_USER
from .utils.assortment import DiscountAssortment
from .ga.api import get_goals_conversion_data, initialize_ga_service, send_email
from .ga.constants import table_id, GA_GOALS, GOALS_NUM_DAYS, CRITICAL_DROP, goals_mailing_list, GA_DATE_FORMAT


@task
def run_google_analytics_api():
    path = os.path.dirname(__file__)
    filename = path + '/ga/api.py'
    cmd = "python {} {}".format(filename, table_id)
    subprocess.call(cmd, shell=True)


@task
def send_assortment_reports(emails=(), **filter):
    refresh_mat_views()

    message = EmailMessage(subject='ASSORTMENT REPORTS', from_email=EMAIL_HOST_USER, to=emails)

    sub_cat_assortment = DiscountAssortment(**filter)
    sub_cat_filename, sub_cat_csv = sub_cat_assortment.create_csv()

    brand_assortment = DiscountAssortment(level='brand', **filter)
    brand_filename, brand_csv = brand_assortment.create_csv()

    competitor_assortment = DiscountAssortment(level='competitor', **filter)
    competitor_filename, competitor_csv = competitor_assortment.create_csv()

    message.attach(sub_cat_filename, sub_cat_csv.getvalue(), 'text/csv')
    message.attach(brand_filename, brand_csv.getvalue(), 'text/csv')
    message.attach(competitor_filename, competitor_csv.getvalue(), 'text/csv')

    print message.send()


@task
def populate_previous_conversion_data():
    current_date = datetime.now()
    for n in xrange(GOALS_NUM_DAYS):
        date = current_date - timedelta(n + 1)
        goal_completion_api_alert.delay(start_date=date.strftime('%Y-%m-%d'), end_date=date.strftime('%Y-%m-%d'))


@task
def goal_completion_api_alert(emails=[]):
    try:
        current_timestamp = datetime.now()
        current_date = current_timestamp.strftime(GA_DATE_FORMAT)
        service, flags = initialize_ga_service()
        results = get_goals_conversion_data(service,
                                            table_id,
                                            start_date=current_date,
                                            end_date=current_date)

        current_hour = int(current_timestamp.strftime('%H'))
        goal_order = dict()
        for goal, value in results['totalsForAllResults'].iteritems():
            goal_key = goal.split(':')
            goal_id = extract_numbers(goal_key[1])
            goal_name = goal_key[1]
            if not goal_id:
                goal_id = 0

            defaults = {'display_name': GA_GOALS[goal_name]['name']}
            goal_order[GA_GOALS[goal_name]['order']] = dict()
            goal, created = Goals.objects.update_or_create(goal_id=goal_id, goal_name=goal_name, defaults=defaults)

            goal_order[GA_GOALS[goal_name]['order']]['goal'] = goal
            goal_order[GA_GOALS[goal_name]['order']]['goal_completion'] = GoalCompletion.objects.create(goal=goal,
                                                                                                        date=current_date,
                                                                                                        hour=current_hour,
                                                                                                        completions=int(
                                                                                                            value))
        for k, v in goal_order.iteritems():
            try:
                predecessor = Goals.objects.get(goal_name=GA_GOALS[v['goal'].goal_name]['predecessor'])
            except Goals.DoesNotExist:
                predecessor = None
            v['goal'].predecessor = predecessor
            v['goal'].save()

        insert_list = []
        for i in results['rows']:
            country = i[0]
            browser = i[1]
            source = i[2]
            # if 'payu' not in source:
            for c in xrange(0, len(i[3:])):
                goal_completion = goal_order[c]['goal_completion']
                insert_list.append(GoalCompletionByCountryBrowser(country=country.encode('utf-8'), browser=browser,
                                                                  goal_completion=goal_completion,
                                                                  source=source,
                                                                  completions=int(i[3:][c])))

        GoalCompletionByCountryBrowser.objects.bulk_create(insert_list)
        update_conversion_ratio(emails=emails)
    except Exception as e:
        send_email(['abhishek.verma03@exclusively.com'], 'Buy Funnel API Error', e.message)


def update_conversion_ratio(emails=[]):
    overall_alerts = []
    browser_alerts = []

    avg_data = Goals.get_avg_goal_completions()
    goals = Goals.objects.exclude(goal_name='goalCompletionsAll')
    total_goals = Goals.objects.get(goal_name='goalCompletionsAll')

    total_goals.overall_average_completions = \
        [i.overall_goal_completions for i in avg_data if i.id == total_goals.id][0]
    total_goals.india_average_completions = [i.india_goal_completions for i in avg_data if i.id == total_goals.id][0]
    total_goals.row_average_completions = [i.row_goal_completions for i in avg_data if i.id == total_goals.id][0]
    total_goals.save()

    for goal in goals:
        goal.overall_average_completions = [i.overall_goal_completions for i in avg_data if i.id == goal.id][0]
        goal.india_average_completions = [i.india_goal_completions for i in avg_data if i.id == goal.id][0]
        goal.row_average_completions = [i.row_goal_completions for i in avg_data if i.id == goal.id][0]
        goal.overall_conversion_ratio = goal.overall_average_completions / \
                                        [i.overall_goal_completions for i in avg_data if i.id == goal.predecessor.id][0]
        goal.india_conversion_ratio = goal.india_average_completions / \
                                      [i.india_goal_completions for i in avg_data if i.id == goal.predecessor.id][0]
        goal.row_conversion_ratio = goal.row_average_completions / \
                                    [i.row_goal_completions for i in avg_data if i.id == goal.predecessor.id][0]

        goal.chrome_conversion_ratio = [i.chrome_conversion_ratio for i in avg_data if i.id == goal.id][0]
        goal.ie_conversion_ratio = [i.ie_conversion_ratio for i in avg_data if i.id == goal.id][0]
        goal.safari_conversion_ratio = [i.safari_conversion_ratio for i in avg_data if i.id == goal.id][0]
        goal.other_browser_conversion_ratio = [i.other_browser_conversion_ratio for i in avg_data if i.id == goal.id][0]

        goal.save()

        diff = get_conversion_difference(goal, avg_data)
        alert_dict = dict()

        if diff['icr_diff'] > 0 and (100 / goal.india_conversion_ratio) * diff['icr_diff'] > CRITICAL_DROP:
            if 'overall' not in alert_dict:
                alert_dict['overall'] = dict()
            alert_dict['overall'][goal.display_name] = dict()
            alert_dict['overall'][goal.display_name]['name'] = 'India'
            alert_dict['overall'][goal.display_name]['drop_percent'] = (100 / goal.india_conversion_ratio) * diff[
                'icr_diff']

        if diff['ocr_diff'] > 0 and (100 / goal.overall_conversion_ratio) * diff['ocr_diff'] > CRITICAL_DROP:
            if 'overall' not in alert_dict:
                alert_dict['overall'] = dict()
            alert_dict['overall'][goal.display_name] = dict()
            alert_dict['overall'][goal.display_name]['name'] = 'Overall'
            alert_dict['overall'][goal.display_name]['drop_percent'] = (100 / goal.overall_conversion_ratio) * diff[
                'ocr_diff']

        if diff['rcr_diff'] > 0 and (100 / goal.row_conversion_ratio) * diff['rcr_diff'] > CRITICAL_DROP:
            if 'overall' not in alert_dict:
                alert_dict['overall'] = dict()
            alert_dict['overall'][goal.display_name] = dict()
            alert_dict['overall'][goal.display_name]['name'] = 'ROW'
            alert_dict['overall'][goal.display_name]['drop_percent'] = (100 / goal.row_conversion_ratio) * diff[
                'rcr_diff']

        if diff['chrome_conversion_ratio_diff'] > 0 and (100 / goal.chrome_conversion_ratio) * diff[
            'chrome_conversion_ratio_diff'] > CRITICAL_DROP:
            if 'browser' not in alert_dict:
                alert_dict['browser'] = dict()
            alert_dict['browser'][goal.display_name] = dict()
            alert_dict['browser'][goal.display_name]['name'] = 'Chrome'
            alert_dict['browser'][goal.display_name]['drop_percent'] = (100 / goal.chrome_conversion_ratio) * diff[
                'chrome_conversion_ratio_diff']

        if diff['ie_conversion_ratio_diff'] > 0 and (100 / goal.ie_conversion_ratio) * diff[
            'ie_conversion_ratio_diff'] > CRITICAL_DROP:
            if 'browser' not in alert_dict:
                alert_dict['browser'] = dict()
            alert_dict['browser'][goal.display_name] = dict()
            alert_dict['browser'][goal.display_name]['name'] = 'Internet Explorer'
            alert_dict['browser'][goal.display_name]['drop_percent'] = (100 / goal.ie_conversion_ratio) * diff[
                'ie_conversion_ratio_diff']

        if diff['safari_conversion_ratio_diff'] > 0 and (100 / goal.safari_conversion_ratio) * diff[
            'safari_conversion_ratio_diff'] > CRITICAL_DROP:
            if 'browser' not in alert_dict:
                alert_dict['browser'] = dict()
            alert_dict['browser'][goal.display_name] = dict()
            alert_dict['browser'][goal.display_name]['name'] = 'Safari'
            alert_dict['browser'][goal.display_name]['drop_percent'] = (100 / goal.safari_conversion_ratio) * diff[
                'safari_conversion_ratio_diff']

        if diff['other_browser_conversion_ratio_diff'] > 0 and (100 / goal.other_browser_conversion_ratio) * diff[
            'other_browser_conversion_ratio_diff'] > CRITICAL_DROP:
            if 'browser' not in alert_dict:
                alert_dict['browser'] = dict()
            alert_dict['browser'][goal.display_name] = dict()
            alert_dict['browser'][goal.display_name]['name'] = 'Other Browser'
            alert_dict['browser'][goal.display_name]['drop_percent'] = (100 / goal.other_browser_conversion_ratio) * \
                                                                       diff[
                                                                           'other_browser_conversion_ratio_diff']

        goal_history = GoalConversionRatioHistory.objects.create(goal=goal,
                                                                 overall_conversion_ratio=goal.overall_conversion_ratio,
                                                                 india_conversion_ratio=goal.india_conversion_ratio,
                                                                 row_conversion_ratio=goal.row_conversion_ratio,
                                                                 current_overall_conversion_ratio=diff[
                                                                     'current_overall_conversion_ratio'],
                                                                 current_india_conversion_ratio=diff[
                                                                     'current_india_conversion_ratio'],
                                                                 current_row_conversion_ratio=diff[
                                                                     'current_row_conversion_ratio'],
                                                                 ocr_diff=diff['ocr_diff'],
                                                                 icr_diff=diff['icr_diff'],
                                                                 rcr_diff=diff['rcr_diff'],
                                                                 chrome_conversion_ratio=goal.chrome_conversion_ratio,
                                                                 ie_conversion_ratio=goal.ie_conversion_ratio,
                                                                 safari_conversion_ratio=goal.safari_conversion_ratio,
                                                                 other_browser_conversion_ratio=goal.other_browser_conversion_ratio,
                                                                 current_chrome_conversion_ratio=diff[
                                                                     'current_chrome_conversion_ratio'],
                                                                 current_ie_conversion_ratio=diff[
                                                                     'current_ie_conversion_ratio'],
                                                                 current_safari_conversion_ratio=diff[
                                                                     'current_safari_conversion_ratio'],
                                                                 current_other_browser_conversion_ratio=diff[
                                                                     'current_other_browser_conversion_ratio'],
                                                                 alerted=alert_dict)
        if alert_dict.get('overall'):
            alert_dict['overall']['history'] = goal_history
            overall_alerts.append(alert_dict['overall'])

        if alert_dict.get('browser'):
            alert_dict['browser']['history'] = goal_history
            browser_alerts.append(alert_dict['browser'])

    send_conversion_drop_alert(overall_alerts, browser_alerts, emails=emails)


def get_conversion_difference(goal, avg_data):
    data = Goals.get_avg_goal_completions(current_hour=True)

    diff = dict()
    diff['current_overall_conversion_ratio'] = [i.overall_goal_completions for i in data if i.id == goal.id][0] / \
                                               [i.overall_goal_completions for i in data if
                                                i.id == goal.predecessor.id][0]

    diff['current_india_conversion_ratio'] = [i.india_goal_completions for i in data if i.id == goal.id][0] / \
                                             [i.india_goal_completions for i in data if
                                              i.id == goal.predecessor.id][0]
    diff['current_row_conversion_ratio'] = [i.row_goal_completions for i in data if i.id == goal.id][0] / \
                                           [i.row_goal_completions for i in data if i.id == goal.predecessor.id][0]

    diff['ocr_diff'] = goal.overall_conversion_ratio - diff['current_overall_conversion_ratio']
    diff['icr_diff'] = goal.india_conversion_ratio - diff['current_india_conversion_ratio']
    diff['rcr_diff'] = goal.row_conversion_ratio - diff['current_row_conversion_ratio']

    diff['current_chrome_conversion_ratio'] = [i.chrome_conversion_ratio for i in data if i.id == goal.id][0]
    diff['chrome_conversion_ratio_diff'] = goal.chrome_conversion_ratio - diff['current_chrome_conversion_ratio']

    diff['current_ie_conversion_ratio'] = [i.ie_conversion_ratio for i in data if i.id == goal.id][0]
    diff['ie_conversion_ratio_diff'] = goal.ie_conversion_ratio - diff['current_ie_conversion_ratio']

    diff['current_safari_conversion_ratio'] = [i.safari_conversion_ratio for i in data if i.id == goal.id][0]
    diff['safari_conversion_ratio_diff'] = goal.safari_conversion_ratio - diff['current_safari_conversion_ratio']

    diff['current_other_browser_conversion_ratio'] = \
        [i.other_browser_conversion_ratio for i in data if i.id == goal.id][0]
    diff['other_browser_conversion_ratio_diff'] = goal.other_browser_conversion_ratio - diff[
        'current_other_browser_conversion_ratio']

    return diff


def send_conversion_drop_alert(overall_alerts, browser_alerts, emails=[]):
    if not emails:
        emails = goals_mailing_list
    subject = 'Buy Flow - Funnel: Drop Alert'
    html = get_template('analytics/goal_conversion_alert.html')

    d = Context({'overall_alerts': overall_alerts, 'browser_alerts': browser_alerts,
                 'datetime': datetime.now().strftime('%I %p %b %d, %Y')})

    html_content = html.render(d)
    msg = EmailMultiAlternatives(subject, 'Goal Conversion Ratio', from_email=EMAIL_HOST_USER,
                                 to=emails)
    msg.attach_alternative(html_content, "text/html")
    msg.send()
