from datetime import datetime

from django.shortcuts import render
from django.views.generic.base import View

from .models import Goals
from ..base.utils import get_json_response
from .utils.assortment import DiscountAssortment
from ..catalogue.models import Category


class AssortmentView(View):
    template_name = 'analytics/assortment.html'

    def dispatch(self, request, *args, **kwargs):
        return super(AssortmentView, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        level = request.GET.get('level', 'competitor')

        order_by = '-total_products'
        if request.GET.get('order_by'):
            order_by = request.GET.get('order_by')

        filter = dict()
        if request.GET.getlist('brand_id'):
            filter['brand_id'] = request.GET.getlist('brand_id')
        if request.GET.getlist('sub_cat_id'):
            filter['sub_cat_id'] = request.GET.getlist('sub_cat_id')
        if request.GET.getlist('competitor_id'):
            filter['competitor_id'] = request.GET.getlist('competitor_id')

        assortment = DiscountAssortment(level=level, page=request.GET.get('page'),
                                        page_size=request.GET.get('page_size'), order_by=order_by,
                                        **filter)

        return render(request, template_name=self.template_name,
                      context={'assortment_data': assortment.data, 'level': level, 'count': assortment.total_results,
                               'page': assortment.page, 'total_pages': assortment.total_pages,
                               'datetime': datetime.now().strftime('%H:%M %b, %d %Y')})


def get_competitor_chart_data(request):
    order_by = '-total_products'
    if request.GET.get('order_by'):
        order_by = request.GET.get('order_by')

    filter = dict()
    if request.GET.getlist('brand_id'):
        filter['brand_id'] = request.GET.getlist('brand_id')
    if request.GET.getlist('sub_cat_id'):
        filter['sub_cat_id'] = request.GET.getlist('sub_cat_id')
    if request.GET.getlist('competitor_id'):
        filter['competitor_id'] = request.GET.getlist('competitor_id')

    assortment = DiscountAssortment(level=request.GET.get('level', 'competitor'), page=request.GET.get('page'),
                                    page_size=request.GET.get('page_size'), order_by=order_by,
                                    **filter)

    # assortment = DiscountAssortment(level='competitor', order_by='-total_products', **filter)
    response = dict()
    response['level'] = request.GET.get('level', 'competitor')
    response['data'] = list()
    for result in assortment.query_result:
        result = list(result)
        bind = dict()
        bind['name'] = result[1][0].upper() + result[1][1:]
        bind['data'] = [result[2]] + result[4:]
        response['data'].append(bind)
    return get_json_response(response)


def get_category_chart_data(request):
    result = Category.get_product_count()
    return get_json_response(result)


def get_goal_conversion_funnel_data(request):
    response = dict()
    date = datetime.now()

    overall_results = Goals.get_goals_completion_count(current_hour=True)

    overall = dict()
    overall['name'] = 'Buy Flow Funnel - {} (Till {})'.format('Overall', date.strftime('%I %p'))
    overall['data'] = [[i['goal_completion__goal__display_name'], i['total_completions']] for i in overall_results]
    response['overall'] = [overall]

    india_results = Goals.get_goals_completion_count(current_hour=True, region='India')
    india = dict()
    india['name'] = 'Buy Flow Funnel - {} (Till {})'.format('India', date.strftime('%I %p'))
    india['data'] = [[i['goal_completion__goal__display_name'], i['total_completions']] for i in india_results]
    response['india'] = [india]

    row_results = Goals.get_goals_completion_count(current_hour=True, region='ROW')
    row = dict()
    row['name'] = 'Buy Flow Funnel - {} (Till {})'.format('ROW', date.strftime('%I %p'))
    row['data'] = [[i['goal_completion__goal__display_name'], i['total_completions']] for i in row_results]
    response['row'] = [row]

    return get_json_response(response)


def get_goal_conversion_funnel_dashboard(request):
    avg_data = Goals.get_avg_goal_completions()
    print avg_data
    current_data = Goals.get_avg_goal_completions(current_hour=True)
    print current_data
    for i in xrange(len(current_data)):
        if i == 0:
            current_data[i].__setattr__('avg_overall_goal_percent', '')
            current_data[i].__setattr__('avg_india_goal_percent', '')
            current_data[i].__setattr__('avg_row_goal_percent', '')
        else:
            current_data[i].__setattr__('avg_overall_goal_percent', avg_data[i].overall_goal_percent)
            current_data[i].__setattr__('avg_india_goal_percent', avg_data[i].india_goal_percent)
            current_data[i].__setattr__('avg_row_goal_percent', avg_data[i].row_goal_percent)

    return render(request, template_name='analytics/goals_conversion.html',
                  context={'avg_data': avg_data, 'current_data': current_data})
