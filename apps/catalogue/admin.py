# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import *


class ProductAdmin(admin.ModelAdmin):
    list_display = (
        'competitor', 'brand', 'sub_category', 'product_id', 'product_name', 'price', 'discounted_price', 'discount')

    search_fields = ['product_name', 'product_id']
    list_filter = ['competitor', 'sub_category']


class BrandAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'display_name')

    search_fields = ['name', 'display_name']


admin.site.register(Brand, BrandAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Category)
