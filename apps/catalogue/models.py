from django.db import models
from django.db.models.aggregates import Count
from django_pgjson.fields import JsonBField

from ..base.models import BaseModel
from ..scraper.models import Competitors


class Category(models.Model):
    name = models.CharField(max_length=100, unique=True, db_index=True)
    parent = models.ForeignKey('self', null=True, blank=True)

    def __unicode__(self):
        return '{}'.format(self.name)

    @classmethod
    def get_product_count(cls):
        products = Product.objects.values('competitor__name', 'sub_category').order_by('-products').annotate(
            products=Count('id'))

        parent_category = [{'name': cat.name, 'sub_cat': [sub_cat.id for sub_cat in cat.category_set.all()]} for cat
                           in cls.objects.filter(parent=None)]
        competitors = sorted([i.name for i in Competitors.objects.all()])

        # result = [{'competitor': comp, 'category': cat['name'],
        #            'products': cls.get_category_product_count(products, parent_category, comp, cat['name'])} for comp in
        #           competitors for cat in parent_category]
        result = dict()
        result['competitors'] = competitors
        result['data'] = [{'name': cat['name'],
                           'data': cls.get_category_product_count(products, parent_category, competitors, cat['name'])}
                          for cat in parent_category]

        return result

    @staticmethod
    def get_category_product_count(products, parent_category_list, competitor, category):
        count_list = list()
        sub_cat_list = [i['sub_cat'] for i in parent_category_list if i['name'] == category][0]

        for c in competitor:
            count = 0
            for p in products:
                if c == p['competitor__name'] and p['sub_category'] in sub_cat_list:
                    count += p['products']
            count_list.append(count)

        return count_list


class Brand(models.Model):
    name = models.CharField(max_length=255, unique=True, db_index=True)
    display_name = JsonBField(null=True)

    # def __unicode__(self):
    #     return '{}'.format(self.name.encode('utf-8'))


class Product(BaseModel):
    product_id = models.CharField(max_length=50, db_index=True)
    competitor = models.ForeignKey('scraper.Competitors')
    product_name = models.CharField(max_length=200)
    brand = models.ForeignKey('catalogue.Brand')
    sub_category = models.ForeignKey('catalogue.Category')
    price = models.FloatField()
    discount = models.FloatField()
    discounted_price = models.FloatField()
    product_url = models.TextField()
    attributes = JsonBField(null=True, blank=True)

    def __unicode__(self):
        return '{}'.format(str(self.product_name.encode('utf-8')))

    class Meta:
        unique_together = ('product_id', 'competitor')
