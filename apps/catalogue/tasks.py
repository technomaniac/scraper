from datetime import datetime, timedelta
import time

from celery.task.base import task

from ..scraper.tasks import refresh_mat_views
from .models import Product


@task
def clear_discount_products(days=7):
    last_updated = datetime.now() - timedelta(days=days)
    timestamp = int(time.mktime(last_updated.timetuple()))
    Product.objects.filter(updated_on__lte=timestamp).delete()

    refresh_mat_views()


@task
def get_goal_completion_data():
    pass
