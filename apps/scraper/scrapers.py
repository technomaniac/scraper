import re

import requests
from bs4 import BeautifulSoup
from celery.contrib.methods import task
from django.db import connection

from .constants import *
from .models import Competitors
from ..catalogue.models import Product, Brand, Category


class BaseSraper(object):
    request_header = {"content-type": "application/json"}
    headers = {
        'User-agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36'}

    def send_request(self, url, json=True):
        r = requests.get(url, headers=self.request_header)

        if r.status_code == 200:
            if json == True:
                return r.json()
            else:
                return r.content
        else:
            raise Exception

    @staticmethod
    def extract_numbers(string, encode=True):
        if encode:
            return ''.join(re.findall(r'\d+', string.encode('utf-8')))
        else:
            return ''.join(re.findall(r'\d+', string))


class UtsavFashionScraper(BaseSraper):
    base_url = UTSAVFASHION_BASE_URL
    utsav_fashion = Competitors.objects.get(name='utsavfashion')

    def __init__(self):
        print('Utsav Fashion Scraper started ...')
        self.populate_sub_categories()
        self.brand, created = Brand.objects.get_or_create(name='utsavfashion')
        self.brand_id = self.brand.pk

    def populate_sub_categories(self):
        url_category_mapping = dict()

        men_footware = Category.objects.get(name="men's-footware")
        for cat in UTSAVFASHION_MEN_FOOTWARE_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=men_footware)
            url_category_mapping[sub_cat.pk] = cat[1]

        women_footware = Category.objects.get(name="women's-footware")
        for cat in UTSAVFASHION_WOMEN_FOOTWARE_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=women_footware)
            url_category_mapping[sub_cat.pk] = cat[1]

        accessories = Category.objects.get(name="accessories")
        for cat in UTSAVFASHION_ACCESSORIES_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=accessories)
            url_category_mapping[sub_cat.pk] = cat[1]

        men_clothing = Category.objects.get(name="men's-clothing")
        for cat in UTSAVFASHION_MEN_CLOTHING_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=men_clothing)
            url_category_mapping[sub_cat.pk] = cat[1]

        women_clothing = Category.objects.get(name="women's-clothing")
        for cat in UTSAVFASHION_WOMEN_CLOTHING_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=women_clothing)
            url_category_mapping[sub_cat.pk] = cat[1]

        config = dict()
        if self.utsav_fashion.config and type(self.utsav_fashion.config) is dict:
            config.update(self.utsav_fashion.config)

        config['categories'] = url_category_mapping
        self.utsav_fashion.config = config
        self.utsav_fashion.save()

    def start(self):
        for cat, url in self.utsav_fashion.config['categories'].iteritems():
            self.fetch_utsav_fashion_category_data.delay(cat, url)

    @task
    def fetch_utsav_fashion_category_data(self, cat, url):

        i = 1
        while True:
            param = '&pg={}'.format(i)
            request_url = url + param

            print('\nPage {}: sending request to {}\n'.format(i, request_url))
            json = self.send_request(request_url)
            if json['items']:
                self.parse_json.delay(json['items'], cat)
                i += 1
            else:
                break

    @task
    def parse_json(self, items, cat):
        for item in items:
            bind = dict()
            bind['competitor_id'] = self.utsav_fashion.pk
            defaults = dict()
            defaults['sub_category_id'] = cat
            defaults['product_name'] = item['name']
            bind['product_id'] = item['code']

            defaults['price'] = float(item['price'])
            defaults['discounted_price'] = float(item['discountprice'])
            defaults['discount'] = round(
                (float(defaults['price'] - defaults['discounted_price']) / float(defaults['price']) * 100), 2)

            defaults['brand_id'] = self.brand_id

            self.create_or_update_utsav_product(bind, defaults)

        print('{} products found'.format(len(items)))

    @task
    def create_or_update_utsav_product(self, bind, defaults):
        product, created = Product.objects.update_or_create(competitor_id=bind['competitor_id'],
                                                            product_id=bind['product_id'],
                                                            defaults=defaults)
        if created:
            string = 'created'
        else:
            string = 'updated'

        print('{} {}'.format(bind['product_id'], string))


class IndianRootsScraper(BaseSraper):
    base_url = INDIAN_ROOTS_BASE_URL
    indian_roots = Competitors.objects.get(name='indianroots')

    def __init__(self):
        print('Indian Roots Scraper started ...')
        self.populate_sub_categories()

    def populate_sub_categories(self):
        url_category_mapping = dict()

        accessories = Category.objects.get(name="accessories")
        for cat in INDIAN_ROOTS_ACCESSORIES_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=accessories)
            url_category_mapping[sub_cat.pk] = cat[1]

        women_footware = Category.objects.get(name="women's-footware")
        for cat in INDIAN_ROOTS_WOMEN_FOOTWARE_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=women_footware)
            url_category_mapping[sub_cat.pk] = cat[1]

        men_clothing = Category.objects.get(name="men's-clothing")
        for cat in INDIAN_ROOTS_MEN_CLOTHING_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=men_clothing)
            url_category_mapping[sub_cat.pk] = cat[1]

        women_clothing = Category.objects.get(name="women's-clothing")
        for cat in INDIAN_ROOTS_WOMEN_CLOTHING_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=women_clothing)
            url_category_mapping[sub_cat.pk] = cat[1]

        config = dict()
        if self.indian_roots.config and type(self.indian_roots.config) is dict:
            config.update(self.indian_roots.config)

        config['categories'] = url_category_mapping
        self.indian_roots.config = config
        self.indian_roots.save()

    def start(self):
        for cat, url in self.indian_roots.config['categories'].iteritems():
            self.fetch_indian_roots_category_data.delay(cat, url)

    @task
    def fetch_indian_roots_category_data(self, cat, url):
        request_url = url
        i = 1
        while True:
            i += 1
            print('\nPage {}: sending request to {}\n'.format(i - 1, request_url))
            r = requests.get(request_url)
            soup = BeautifulSoup(r.content)

            self.parse_indian_roots_html.delay(r.content, cat)

            pager = soup.find('div', {'class': 'pager'})
            if pager:
                pages_li = pager.find_all('li')

                if len(pages_li) > 0:
                    if pages_li[-1].get('class') == ['current']:
                        break
                else:
                    break
            else:
                break

            request_url = '{}/page/{}'.format(url, i)

    @task
    def parse_indian_roots_html(self, content, cat):
        soup = BeautifulSoup(content)
        product_divs = soup.find_all('div', {'class': 'productList'})
        for product_div in product_divs:
            bind = dict()
            bind['competitor_id'] = self.indian_roots.pk
            defaults = dict()
            defaults['sub_category_id'] = cat

            title_div = product_div.find('div', {'class': 'productTitle'}).find_all('a')
            defaults['product_name'] = title_div[0].text.encode('utf-8').strip()
            defaults['product_url'] = title_div[0].get('href').encode('utf-8').strip()
            brand_name = title_div[1].text.encode('utf-8').strip().lower()
            defaults['brand'], created = Brand.objects.get_or_create(name=brand_name)

            price_box = product_div.find('span', {'class': 'regular-price'})
            if price_box:
                bind['product_id'] = self.extract_numbers(price_box.get('id'))
                defaults['discounted_price'] = int(self.extract_numbers(price_box.text))
                defaults['price'] = defaults['discounted_price']
                defaults['discount'] = 0
            else:
                old_price = product_div.find('p', {'class': 'old-price'})
                bind['product_id'] = self.extract_numbers(old_price.find('span').get('id'))
                defaults['price'] = int(self.extract_numbers(old_price.text))

                special_price = product_div.find('p', {'class': 'special-price'})
                defaults['discounted_price'] = int(self.extract_numbers(special_price.text))

                defaults['discount'] = round(
                    (float(defaults['price'] - defaults['discounted_price']) / float(defaults['price']) * 100),
                    2)

            self.create_or_update_indian_roots_product(bind, defaults)
        print('{} products found'.format(len(product_divs)))

    @task
    def create_or_update_indian_roots_product(self, bind, defaults):
        product, created = Product.objects.update_or_create(competitor_id=bind['competitor_id'],
                                                            product_id=bind['product_id'],
                                                            defaults=defaults)
        if created:
            string = 'created'
        else:
            string = 'updated'

        print('{} {}'.format(bind['product_id'], string))


class AmazonScraper(BaseSraper):
    base_url = AMAZON_BASE_URL
    amazon = Competitors.objects.get(name='amazon')
    headers = {
        'User-agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36'}

    def __init__(self):
        print('Amazon Scraper started ...')
        self.populate_sub_categories()

    def populate_sub_categories(self):
        url_category_mapping = dict()

        accessories = Category.objects.get(name="accessories")
        for cat in AMAZON_ACCESSORIES_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=accessories)
            url_category_mapping[sub_cat.pk] = cat[1]

        women_footware = Category.objects.get(name="women's-footware")
        for cat in AMAZON_WOMEN_FOOTWARE_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=women_footware)
            url_category_mapping[sub_cat.pk] = cat[1]

        men_footware = Category.objects.get(name="men's-footware")
        for cat in AMAZON_MEN_FOOTWARE_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=men_footware)
            url_category_mapping[sub_cat.pk] = cat[1]

        watches = Category.objects.get(name="watches")
        for cat in AMAZON_WATCHES_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=watches)
            url_category_mapping[sub_cat.pk] = cat[1]

        men_clothing = Category.objects.get(name="men's-clothing")
        for cat in AMAZON_MEN_CLOTHING_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=men_clothing)
            url_category_mapping[sub_cat.pk] = cat[1]

        women_clothing = Category.objects.get(name="women's-clothing")
        for cat in AMAZON_WOMEN_CLOTHING_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=women_clothing)
            url_category_mapping[sub_cat.pk] = cat[1]

        config = dict()
        if self.amazon.config and type(self.amazon.config) is dict:
            config.update(self.amazon.config)

        config['categories'] = url_category_mapping
        self.amazon.config = config
        self.amazon.save()

    def start(self):
        amazon = Competitors.objects.get(name='amazon')
        s = requests.session()
        s.headers.update(self.headers)
        s.get(AMAZON_BASE_URL)
        for cat, url in amazon.config['categories'].iteritems():
            print('Category : {}, URL: {}\n'.format(cat, url))
            self.fetch_amazon_category_data.delay(s, cat, url)

    @task
    def fetch_amazon_category_data(self, s, cat, url):
        request_url = url
        i = 0
        while True:
            i += 1
            print('\nPage - {}: sending request to {}\n'.format(i, request_url))
            r = s.get(request_url)
            content = r.content

            soup = BeautifulSoup(content)
            items = soup.find_all('li', {'data-action': 'sx-detail-display-trigger'})
            print('\n{} products found\n URL : {}\n'.format(len(items), url))
            if items:
                self.parse_amazon_html(items, cat)

            next_page = soup.find('a', {'id': 'pagnNextLink'})

            if next_page:
                request_url = self.base_url + next_page.get('href').encode('utf-8')
            else:
                break

    @task
    def parse_amazon_html(self, items, cat):
        # soup = BeautifulSoup(content)
        # item_ul = soup.find('ul', {'id': 's-results-list-atf'})
        for item in items:
            try:
                product_id = item.get('data-asin').encode('utf-8').strip()
                item_container = item.find('div', {'class': 's-item-container'})

                item_divs = item_container.find_all('div', {'class': 'a-row a-spacing-none'}, recursive=False)
                a = item_divs[0].find('a', {'class': 's-access-detail-page'})

                defaults = dict()
                defaults['sub_category_id'] = cat
                defaults['product_name'] = a.get('title').encode('utf-8').strip()
                url = a.get('href').encode('utf-8').strip()

                if re.match(r'^(http|https)://', url):
                    defaults['product_url'] = url
                else:
                    defaults['product_url'] = self.base_url + url

                pdp_content = self.send_request(defaults['product_url'], json=False)
                pdp_soup = BeautifulSoup(pdp_content)
                brand_div = pdp_soup.find('div', {'id': 'brandByline_feature_div'})
                brand_name = brand_div.find('a', {'id': 'brand'}).text.encode('utf-8').lower().strip()

                defaults['brand'], created = Brand.objects.get_or_create(name=brand_name)

                price_divs = item_divs[0].find_all('div', {'class': 'a-row a-spacing-none'}, recursive=False)
                disc_price_span = price_divs[0].find('span', {'class': 'a-color-price'})

                disc_price_text = disc_price_span.text.encode('utf-8').strip()
                disc_price_text = disc_price_text.split('-')[0]
                if len(disc_price_text.split('.')):
                    disc_price = disc_price_text.split('.')[0]
                else:
                    disc_price = disc_price_text
                defaults['discounted_price'] = int(self.extract_numbers(disc_price, encode=False))

                price_span = price_divs[0].find('span', {'class': 'a-size-small a-color-secondary a-text-strike'})
                if price_span:
                    price_text = price_span.text.encode('utf-8').strip()
                    if len(price_text.split('.')):
                        price = price_text.split('.')[0]
                    else:
                        price = price_text
                    defaults['price'] = int(self.extract_numbers(price, encode=False))
                    defaults['discount'] = round(
                        (float(defaults['price'] - defaults['discounted_price']) / float(defaults['price']) * 100),
                        2)
                else:
                    defaults['price'] = defaults['discounted_price']
                    defaults['discount'] = 0
                # print defaults
                self.create_or_update_amazon_product.apply_async(args=[self.amazon.pk, product_id, defaults],
                                                                 queue='creator')

            except Exception as e:
                print e.message
                pass

    @task
    def create_or_update_amazon_product(self, amazon_id, product_id, defaults):
        product, created = Product.objects.update_or_create(competitor_id=amazon_id,
                                                            product_id=product_id,
                                                            defaults=defaults)
        if created:
            string = 'created'
        else:
            string = 'updated'

        print('{} {}'.format(product_id, string))


class MyntraScraper(BaseSraper):
    base_url = MYNTRA_SEARCH_URL
    myntra = Competitors.objects.get(name='myntra')
    competitor_id = myntra.pk

    def __init__(self):
        print('Myntra Scraper started ...')
        self.populate_sub_categories()

    def start(self):

        for cat, url in self.myntra.config['categories'].iteritems():
            self.fetch_myntra_category.delay(cat, url)

    @task
    def fetch_myntra_category(self, cat, url):
        i = 1
        while True:
            try:
                get_params = '?userQuery=false&p={}&sort=discount'.format(i)
                request_url = url + get_params

                print('\nsending requests to - {}\n'.format(request_url))
                data = self.send_request(request_url)

                if data['data']['results']['products']:
                    self.dump_data.delay(data['data']['results']['products'], cat)
                else:
                    break

                i += 1
            except Exception as e:
                print (e.message)

    @task
    def dump_data(self, data_set, cat_id):

        for data in data_set:
            try:
                brand, created = Brand.objects.get_or_create(
                    name=data['brands_filter_facet'].encode('utf-8').lower().strip())

                bind = dict()
                bind['competitor_id'] = self.competitor_id
                bind['product_id'] = data['styleid']

                defaults = dict()
                defaults['sub_category_id'] = cat_id
                defaults['product_name'] = data['product'].encode('utf-8')
                defaults['price'] = data['price']
                defaults['discount'] = round(
                    (float(data['price'] - data['discounted_price']) / float(data['price']) * 100),
                    2)
                defaults['discounted_price'] = data['discounted_price']
                defaults['brand'] = brand
                defaults['product_url'] = MYNTRA_HOSTNAME + 'style/{}/'.format(data['styleid'])

                attr = dict()
                attr['color'] = data['global_attr_base_colour'].encode('utf-8')
                attr['sizes'] = data['sizes']
                attr['image_url'] = data['search_image'].encode('utf-8')

                defaults['attributes'] = attr

                self.create_product.apply_async(args=[bind, defaults], queue='creator')
            except Exception as e:
                print e.message

    @task
    def create_product(self, bind, defaults):
        product, created = Product.objects.update_or_create(competitor_id=bind['competitor_id'],
                                                            product_id=bind['product_id'],
                                                            defaults=defaults)
        if created:
            str = 'created'
        else:
            str = 'updated'

        print('{} product_id {}'.format(bind['product_id'], str))

    def populate_sub_categories(self):
        url_category_mapping = dict()

        watches = Category.objects.get(name="watches")
        for cat in MYNTRA_WATCHES_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat, parent=watches)
            url_category_mapping[sub_cat.pk] = '{}{}'.format(self.base_url, cat)

        accessories = Category.objects.get(name="accessories")
        for cat in MYNTRA_ACCESSORIES_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat, parent=accessories)
            url_category_mapping[sub_cat.pk] = '{}{}'.format(self.base_url, cat)

        men_footware = Category.objects.get(name="men's-footware")
        for cat in MYNTRA_MEN_FOOTWARE_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat, parent=men_footware)
            url_category_mapping[sub_cat.pk] = '{}{}'.format(self.base_url, cat)

        women_footware = Category.objects.get(name="women's-footware")
        for cat in MYNTRA_WOMEN_FOOTWARE_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat, parent=women_footware)
            url_category_mapping[sub_cat.pk] = '{}{}'.format(self.base_url, cat)

        men_clothing = Category.objects.get(name="men's-clothing")
        for cat in MYNTRA_MEN_CLOTHING_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat, parent=men_clothing)
            url_category_mapping[sub_cat.pk] = '{}{}'.format(self.base_url, cat)

        women_clothing = Category.objects.get(name="women's-clothing")
        for cat in MYNTRA_WOMEN_CLOTHING_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat, parent=women_clothing)
            url_category_mapping[sub_cat.pk] = '{}{}'.format(self.base_url, cat)

        config = dict()
        if self.myntra.config and type(self.myntra.config) is dict:
            config.update(self.myntra.config)

        config['categories'] = url_category_mapping
        self.myntra.config = config
        self.myntra.save()


class FlipkartScraper(BaseSraper):
    request_header = {}
    base_url = FLIPKART_PRODUCT_LISTING_URL
    pdp_base_url = 'http://flipkart.com'
    flipkart = Competitors.objects.get(name='flipkart')
    competitor_id = flipkart.pk

    def __init__(self):
        print('\nFlipkart Scraper started ... \n\n')
        self.populate_sub_categories()
        # self.populate_brands()
        # self.brands = self.get_brand_list()

    def get_brand_list(self):
        sql = '''select * from catalogue_brand where display_name ? '{}';'''.format(self.competitor_id)
        cursor = connection.cursor()
        cursor.execute(sql)
        query_result = cursor.fetchall()

        result = list()
        for row in query_result:
            if type(row[2][str(self.competitor_id)]) is list:
                result.extend([{'id': row[0], 'name': i.encode('utf-8')} for i in row[2][str(self.competitor_id)]])
            else:
                result.append({'id': row[0], 'name': row[2][str(self.competitor_id).encode('utf-8')]})
        return result

    def start(self):
        s = requests.session()
        s.headers.update(self.headers)
        s.get(self.pdp_base_url)
        for cat_id, cat_dict in self.flipkart.config['categories'].iteritems():
            self.fetch_flipkart_category_data.delay(s, cat_dict, cat_id)

    @task
    def fetch_flipkart_category_data(self, s, cat_dict, cat_id):
        print('\n Starting Sub-Category : {}\n'.format(cat_dict['name']))
        brands = self.populate_brands(s, cat_dict['url'])
        for brand in brands:
            print('\n Starting Brand : {}\n'.format(brand.name))
            start = 0
            while True:
                get_params = '?{}&start={}&p%5B%5D=facets.brand%255B%255D%3D{}&ajax=true'.format(
                    cat_dict['get_url'],
                    start,
                    brand.display_name[str(self.competitor_id)])

                request_url = self.base_url + get_params

                print('\nsending requests to - {}\n'.format(request_url))
                try:
                    r = s.get(request_url)
                    soup = BeautifulSoup(r.content)
                    if soup.find_all("div", {"class": "errorImage"}):
                        break

                    data = soup.find_all("div", {"data-ctrl": "ProductUnitController"})
                    print('-------------', len(data), 'products found ----')
                    if len(data) > 0:

                        self.parse_flipkart_html.delay(r.content, cat_id, brand.pk)
                        start += len(data) + 1

                    else:
                        break
                except Exception as e:
                    print e.message

    @task
    def parse_flipkart_html(self, content, cat_id, brand_id):
        soup = BeautifulSoup(content)
        data = soup.find_all("div", {"data-ctrl": "ProductUnitController"})

        bind = dict()
        bind['competitor_id'] = self.competitor_id
        for item in data:
            try:
                bind['product_id'] = item.get('data-pid').encode('utf-8')

                defaults = dict()
                defaults['sub_category_id'] = cat_id
                name = item.find_all("a", {"class": "fk-display-block"})[0]
                defaults['product_name'] = name.get('title').encode('utf-8')

                discounted_price = item.find_all("div", {"class": "pu-final"})[0]
                defaults['discounted_price'] = int(self.extract_numbers(discounted_price.text))
                #
                price = item.find_all("span", {"class": "pu-old"})
                if price:
                    defaults['price'] = int(self.extract_numbers(price[0].text))
                    dis_per = item.find_all("span", {"class": "pu-off-per"})
                    defaults['discount'] = float(self.extract_numbers(dis_per[0].text))
                else:
                    defaults['price'] = defaults['discounted_price']
                    defaults['discount'] = 0

                defaults['brand_id'] = brand_id
                defaults['product_url'] = self.pdp_base_url + name.get("href").encode('utf-8')
                # print(bind, defaults)
                self.create_or_update_flipkart_product.apply_async(args=[bind, defaults], queue='creator')
            except Exception as e:
                print e.message

    @task
    def create_or_update_flipkart_product(self, bind, defaults):
        product, created = Product.objects.update_or_create(competitor_id=bind['competitor_id'],
                                                            product_id=bind['product_id'],
                                                            defaults=defaults)
        if created:
            string = 'created'
        else:
            string = 'updated'

        print('{} {}'.format(bind['product_id'], string))

    def populate_sub_categories(self):
        url_category_mapping = dict()

        accessories = Category.objects.get(name="accessories")
        for cat in FLIPKART_ACCESSORIES_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat['name'], parent=accessories)
            url_category_mapping[sub_cat.pk] = cat

        watches = Category.objects.get(name="watches")
        for cat in FLIPKART_WATCHES_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat['name'], parent=watches)
            url_category_mapping[sub_cat.pk] = cat

        women_clothing = Category.objects.get(name="women's-clothing")
        for cat in FLIPKART_WOMEN_CLOTHING_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat['name'], parent=women_clothing)
            url_category_mapping[sub_cat.pk] = cat

        men_footware = Category.objects.get(name="men's-footware")
        for cat in FLIPKART_MEN_FOOTWARE_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat['name'], parent=men_footware)
            url_category_mapping[sub_cat.pk] = cat

        women_footware = Category.objects.get(name="women's-footware")
        for cat in FLIPKART_WOMEN_FOOTWARE_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat['name'], parent=women_footware)
            url_category_mapping[sub_cat.pk] = cat

        mens_clothing = Category.objects.get(name="men's-clothing")
        for cat in FLIPKART_MEN_CLOTHING_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat['name'], parent=mens_clothing)
            url_category_mapping[sub_cat.pk] = cat

        config = dict()
        if self.flipkart.config and type(self.flipkart.config) is dict:
            config.update(self.flipkart.config)

        config['categories'] = url_category_mapping
        self.flipkart.config = config
        self.flipkart.save()

    def populate_brands(self, s, url):
        r = s.get(url, headers=self.request_header)
        content = r.content
        soup = BeautifulSoup(content)
        brand_ui = soup.find('ul', {'id': 'brand'})
        brands = brand_ui.find_all('li', {'class': 'facet'})
        print('\npopulating brands ...\n\n')
        brand_queryset = list()
        for brand in brands:
            brand_name = brand.get('title').encode('utf-8').lower().strip()[:50]
            facet_value = brand.find('input').get('value').encode('utf-8').split('=')

            defaults = dict()
            defaults['display_name'] = {str(self.competitor_id): facet_value[1]}
            brand, created = Brand.objects.update_or_create(name=brand_name, defaults=defaults)
            brand_queryset.append(brand)
        return brand_queryset


class PerniaScraper(BaseSraper):
    pernia = Competitors.objects.get(name='pernia')

    def __init__(self):
        self.populate_sub_categories()

    def populate_sub_categories(self):
        url_category_mapping = dict()

        men_clothing = Category.objects.get(name="men's-clothing")
        for cat in PERNIA_MEN_CLOTHING_SUB_CAT:
            sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=men_clothing)
            url_category_mapping[sub_cat.pk] = PERNIA_BASE_URL + '/mens-shop'

        women_clothing = Category.objects.get(name="women's-clothing")
        for cat in PERNIA_WOMEN_CLOTHING_SUB_CAT:
            if type(cat) is tuple:
                sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=women_clothing)
                url_category_mapping[sub_cat.pk] = PERNIA_BASE_URL + '/clothing/' + cat[1]
            else:
                sub_cat, created = Category.objects.get_or_create(name=cat, parent=women_clothing)
                url_category_mapping[sub_cat.pk] = PERNIA_BASE_URL + '/clothing/' + cat

        accessories = Category.objects.get(name="accessories")
        for cat in PERNIA_ACCESSORIES_SUB_CAT:
            if type(cat) is tuple:
                sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=accessories)
                if len(cat) > 2:
                    url_category_mapping[sub_cat.pk] = PERNIA_BASE_URL + '/' + cat[2] + '/' + cat[1]
                else:
                    url_category_mapping[sub_cat.pk] = PERNIA_BASE_URL + '/accessories/' + cat[1]
            else:
                sub_cat, created = Category.objects.get_or_create(name=cat, parent=accessories)
                url_category_mapping[sub_cat.pk] = PERNIA_BASE_URL + '/accessories/' + cat

        women_footware = Category.objects.get(name="women's-footware")
        for cat in PERNIA_WOMEN_FOOTWARE_SUB_CAT:
            if type(cat) is tuple:
                sub_cat, created = Category.objects.get_or_create(name=cat[0], parent=women_footware)
                if len(cat) > 2:
                    url_category_mapping[sub_cat.pk] = PERNIA_BASE_URL + '/' + cat[2] + '/' + cat[1]
                else:
                    url_category_mapping[sub_cat.pk] = PERNIA_BASE_URL + '/accessories/' + cat[1]
            else:
                sub_cat, created = Category.objects.get_or_create(name=cat, parent=accessories)
                url_category_mapping[sub_cat.pk] = PERNIA_BASE_URL + '/accessories/' + cat

        config = dict()
        if self.pernia.config and type(self.pernia.config) is dict:
            config.update(self.pernia.config)

        config['categories'] = url_category_mapping
        self.pernia.config = config
        self.pernia.save()

        print('\nPernia categories created/updated')

    def start(self):
        print('\nPernia Scraper started ... \n\n')
        s = requests.session()
        s.get(PERNIA_BASE_URL)

        for cat, url in self.pernia.config['categories'].iteritems():
            self.fetch_pernia_category_data(s, cat, url)

    @task
    def fetch_pernia_category_data(self, s, cat, url):

        i = 1
        last_product = None
        while True:
            # try:
            get_params = '?p={}'.format(i)
            request_url = url + get_params

            print('\n\nsending requests to - {}\n'.format(request_url))

            headers = dict()
            headers['X-Requested-With'] = 'XMLHttpRequest'
            if i > 1:
                headers['Referer'] = url + '?p={}'.format(i - 1)

            r = s.get(request_url, headers=headers)

            content = r.content
            soup = BeautifulSoup(content)
            items = soup.find_all("li", {"class": "item"})

            next_last_product = self.parse_data(cat, items)
            if last_product == next_last_product:
                break
            else:
                last_product = next_last_product
                i += 1


                # except Exception as e:
                #     print e.message

    @task
    def parse_data(self, cat, items):

        print('\n---- {} new products found -----\n\n'.format((len(items))))
        last_product = None
        for item in items:
            bind = dict()
            defaults = dict()
            defaults['sub_category_id'] = cat

            bind['competitor_id'] = self.pernia.pk

            name = item.find('h2', {'class': 'product-name'})
            a = name.find('a')
            brand_name = a.get('title').encode('utf-8').lower().strip()

            brand, created = Brand.objects.get_or_create(name=brand_name)
            defaults['brand'] = brand
            defaults['product_url'] = a.get('href').encode('utf-8')
            bind['product_id'] = defaults['product_url'].split('-')[-1].split('.')[0]
            last_product = bind['product_id']

            defaults['product_name'] = item.find('div', {'class', 'short-description'}).text.encode(
                'utf-8').strip()

            price_div = item.find('div', {'class': 'prdct-price'})
            price_span = price_div.find('span', {'class': 'regular-price'})
            if price_span:
                defaults['price'] = int(self.extract_numbers(price_span.text))
                defaults['discounted_price'] = defaults['price']
                defaults['discount'] = 0
            else:
                price_span = price_div.find('p', {'class': 'old-price'})
                defaults['price'] = int(self.extract_numbers(price_span.text))

                disc_price_span = price_div.find('p', {'class': 'special-price'})
                defaults['discounted_price'] = int(self.extract_numbers(disc_price_span.text))

                defaults['discount'] = round((float(
                    defaults['price'] - defaults['discounted_price']) / float(defaults['price']) * 100),
                                             2)

            self.create_or_update_pernia_product(bind, defaults)

        return last_product

    @task
    def create_or_update_pernia_product(self, bind, defaults):
        product, created = Product.objects.update_or_create(competitor_id=bind['competitor_id'],
                                                            product_id=bind['product_id'],
                                                            defaults=defaults)
        if created:
            string = 'created'
        else:
            string = 'updated'

        print('{} {}'.format(bind['product_id'], string))
