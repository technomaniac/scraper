from django.contrib import admin

# Register your models here.
from .models import Competitors

admin.site.register(Competitors)
