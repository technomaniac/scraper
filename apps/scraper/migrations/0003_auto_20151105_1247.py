# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scraper', '0002_competitors_config'),
    ]

    operations = [
        migrations.AlterField(
            model_name='competitors',
            name='name',
            field=models.CharField(max_length=20, db_index=True),
        ),
    ]
