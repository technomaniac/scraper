from django.db import models
# Create your models here.
from django_pgjson.fields import JsonBField


class Competitors(models.Model):
    name = models.CharField(max_length=20, db_index=True)
    config = JsonBField(null=True, blank=True)

    def __unicode__(self):
        return '{}'.format(self.name)
