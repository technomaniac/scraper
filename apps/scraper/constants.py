MYNTRA_SEARCH_URL = 'http://developer.myntra.com/search/data/'

MYNTRA_HOSTNAME = 'http://developer.myntra.com/'

PERNIA_MEN_CLOTHING_SUB_CAT = ['men-designer-cloths']

FLIPKART_MEN_FOOTWARE_SUB_CAT = [{'name': 'men-flip-flops', 'get_url': 'sid=osp,cil,e1r',
                                  'url': 'http://www.flipkart.com/mens-footwear/slippers-flip-flops/pr?sid=osp%2Ccil%2Ce1r&filterNone=true'},
                                 {'name': 'men-casual-shoes', 'get_url': 'sid=osp,cil,e1f',
                                  'url': 'http://www.flipkart.com/mens-footwear/casual-shoes/pr?sid=osp%2Ccil%2Ce1f&filterNone=true'},
                                 {'name': 'men-sports-shoes', 'get_url': 'sid=osp,cil,1cu',
                                  'url': 'http://www.flipkart.com/mens-footwear/sports-shoes/pr?sid=osp%2Ccil%2C1cu&filterNone=true'},
                                 {'name': 'men-formal-shoes', 'get_url': 'sid=osp,cil,ssb',
                                  'url': 'http://www.flipkart.com/mens-footwear/formal-shoes/pr?sid=osp%2Ccil%2Cssb&filterNone=true'},
                                 {'name': 'men-sandals', 'get_url': 'sid=osp,cil,e83',
                                  'url': 'http://www.flipkart.com/mens-footwear/sandals-floaters/pr?sid=osp%2Ccil%2Ce83&filterNone=true'},
                                 {'name': 'men-ethnic-shoes', 'get_url': 'sid=osp,cil,ssd',
                                  'url': 'http://www.flipkart.com/mens-footwear/ethnic-shoes/pr?sid=osp%2Ccil%2Cssd&filterNone=true'}]

MYNTRA_WOMEN_FOOTWARE_SUB_CAT = ['women-heels', 'women-flats', 'women-casual-shoes', 'women-sports-shoes',
                                 'women-boots', 'women-slippers', 'women-socks']

PERNIA_WOMEN_FOOTWARE_SUB_CAT = [('women-sandals', 'sandals', 'accessories'),
                                 ('women-designer-shoes', 'shoes', 'accessories')]

AMAZON_WOMEN_FOOTWARE_SUB_CAT = [('women-socks',
                                  'http://www.amazon.in/s/ref=lp_1953602031_nr_n_15?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1953602031%2Cn%3A1968517031&bbn=1953602031&ie=UTF8&qid=1447914762&rnid=1953602031'),
                                 ('women-sports-shoes',
                                  'http://www.amazon.in/s/ref=lp_1983578031_nr_n_11?fst=as%3Aoff&rh=n%3A1571283031%2Cn%3A!1571284031%2Cn%3A1983396031%2Cn%3A1983578031%2Cn%3A1983579031&bbn=1983578031&ie=UTF8&qid=1447913398&rnid=1983578031'),
                                 ('women-casual-shoes',
                                  'http://www.amazon.in/s/ref=lp_1983578031_nr_n_10?fst=as%3Aoff&rh=n%3A1571283031%2Cn%3A!1571284031%2Cn%3A1983396031%2Cn%3A1983578031%2Cn%3A1983640031&bbn=1983578031&ie=UTF8&qid=1447913398&rnid=1983578031'),
                                 ('women-heels',
                                  'http://www.amazon.in/s/ref=lp_1983578031_nr_n_9?fst=as%3Aoff&rh=n%3A1571283031%2Cn%3A!1571284031%2Cn%3A1983396031%2Cn%3A1983578031%2Cn%3A1983631031&bbn=1983578031&ie=UTF8&qid=1447913398&rnid=1983578031'),
                                 ('women-loafers',
                                  'http://www.amazon.in/s/ref=lp_1983578031_nr_n_8?fst=as%3Aoff&rh=n%3A1571283031%2Cn%3A!1571284031%2Cn%3A1983396031%2Cn%3A1983578031%2Cn%3A1983635031&bbn=1983578031&ie=UTF8&qid=1447913398&rnid=1983578031'),
                                 ('women-formal-shoes',
                                  'http://www.amazon.in/s/ref=lp_1983578031_nr_n_7?fst=as%3Aoff&rh=n%3A1571283031%2Cn%3A!1571284031%2Cn%3A1983396031%2Cn%3A1983578031%2Cn%3A1983634031&bbn=1983578031&ie=UTF8&qid=1447913398&rnid=1983578031'),
                                 ('women-slippers',
                                  'http://www.amazon.in/s/ref=lp_1983578031_nr_n_6?fst=as%3Aoff&rh=n%3A1571283031%2Cn%3A!1571284031%2Cn%3A1983396031%2Cn%3A1983578031%2Cn%3A1983638031&bbn=1983578031&ie=UTF8&qid=1447913398&rnid=1983578031'),
                                 ('women-sandals',
                                  'http://www.amazon.in/s/ref=lp_1983578031_nr_n_5?fst=as%3Aoff&rh=n%3A1571283031%2Cn%3A!1571284031%2Cn%3A1983396031%2Cn%3A1983578031%2Cn%3A1983633031&bbn=1983578031&ie=UTF8&qid=1447913398&rnid=1983578031'),
                                 ('women-ethnic-shoes',
                                  'http://www.amazon.in/s/ref=lp_1983578031_nr_n_4?fst=as%3Aoff&rh=n%3A1571283031%2Cn%3A!1571284031%2Cn%3A1983396031%2Cn%3A1983578031%2Cn%3A4068645031&bbn=1983578031&ie=UTF8&qid=1447913398&rnid=1983578031'),
                                 ('women-slippers',
                                  'http://www.amazon.in/s/ref=lp_1983578031_nr_n_3?fst=as%3Aoff&rh=n%3A1571283031%2Cn%3A!1571284031%2Cn%3A1983396031%2Cn%3A1983578031%2Cn%3A1983630031&bbn=1983578031&ie=UTF8&qid=1447913398&rnid=1983578031'),
                                 ('women-casual-shoes',
                                  'http://www.amazon.in/s/ref=lp_1983578031_nr_n_2?fst=as%3Aoff&rh=n%3A1571283031%2Cn%3A!1571284031%2Cn%3A1983396031%2Cn%3A1983578031%2Cn%3A1983639031&bbn=1983578031&ie=UTF8&qid=1447913398&rnid=1983578031'),
                                 ('women-boots',
                                  'http://www.amazon.in/s/ref=lp_1983578031_nr_n_1?fst=as%3Aoff&rh=n%3A1571283031%2Cn%3A!1571284031%2Cn%3A1983396031%2Cn%3A1983578031%2Cn%3A1983629031&bbn=1983578031&ie=UTF8&qid=1447913398&rnid=1983578031'),
                                 ('women-flats',
                                  'http://www.amazon.in/s/ref=lp_1983578031_nr_n_0?fst=as%3Aoff&rh=n%3A1571283031%2Cn%3A!1571284031%2Cn%3A1983396031%2Cn%3A1983578031%2Cn%3A1983627031&bbn=1983578031&ie=UTF8&qid=1447913398&rnid=1983578031')]

FLIPKART_WOMEN_FOOTWARE_SUB_CAT = [{'name': 'women-flats', 'get_url': 'sid=osp,iko,9d5',
                                    'url': 'http://www.flipkart.com/womens-footwear/flats/pr?sid=osp%2Ciko%2C9d5&filterNone=true'},
                                   {'name': 'women-ballerinas', 'get_url': 'sid=osp,iko,974',
                                    'url': 'http://www.flipkart.com/womens-footwear/ballerinas/pr?sid=osp%2Ciko%2C974&filterNone=true'},
                                   {'name': 'women-heels', 'get_url': 'sid=osp,iko,6q1',
                                    'url': 'http://www.flipkart.com/womens-footwear/heels/pr?sid=osp%2Ciko%2C6q1&filterNone=true'},
                                   {'name': 'women-wedges', 'get_url': 'sid=osp,iko,jpm',
                                    'url': 'http://www.flipkart.com/womens-footwear/wedges/pr?sid=osp%2Ciko%2Cjpm&filterNone=true'},
                                   {'name': 'women-formal-shoes', 'get_url': 'sid=osp,iko,2ox',
                                    'url': 'http://www.flipkart.com/womens-footwear/formals/pr?sid=osp%2Ciko%2C2ox&filterNone=true'},
                                   {'name': 'women-casual-shoes', 'get_url': 'sid=osp,iko,sx7',
                                    'url': 'http://www.flipkart.com/womens-footwear/casual-shoes/pr?sid=osp%2Ciko%2Csx7&filterNone=true'},
                                   {'name': 'women-sports-shoes', 'get_url': 'sid=osp,iko,d20',
                                    'url': 'http://www.flipkart.com/womens-footwear/sports-shoes/pr?sid=osp%2Ciko%2Cd20&filterNone=true'},
                                   {'name': 'women-sports-sandals', 'get_url': 'sid=osp,iko,ojy',
                                    'url': 'http://www.flipkart.com/womens-footwear/sports-sandals/pr?sid=osp%2Ciko%2Cojy&filterNone=true'},
                                   {'name': 'women-slippers', 'get_url': 'sid=osp,iko,iz7',
                                    'url': 'http://www.flipkart.com/womens-footwear/slippers-flip-flops/pr?sid=osp%2Ciko%2Ciz7&filterNone=true'},
                                   {'name': 'women-sandals', 'get_url': 'sid=osp,iko,riq',
                                    'url': 'http://www.flipkart.com/womens-footwear/sandals/pr?sid=osp%2Ciko%2Criq&filterNone=true'}]

FLIPKART_WOMEN_CLOTHING_SUB_CAT = [{'name': 'women-ethnic-wear', 'get_url': 'sid=2oq,c1r,3pj',
                                    'url': 'http://www.flipkart.com/womens-clothing/ethnic-wear/pr?sid=2oq%2Cc1r%2C3pj&otracker=clp_womens-clothing-ethnic-wear_CategoryLinksModule_0-2_catergorylinks_10_SeeAll'},
                                   {'name': 'women-western-wear', 'get_url': 'sid=2oq,c1r,ha6',
                                    'url': 'http://www.flipkart.com/womens-clothing/western-wear/pr?sid=2oq%2Cc1r%2Cha6&filterNone=true'},
                                   {'name': 'women-swimwear', 'get_url': 'sid=sid=2oq,c1r,tbt',
                                    'url': 'http://www.flipkart.com/womens-clothing/lingerie-sleep-swimwear/pr?sid=2oq%2Cc1r%2Ctbt&filterNone=true'},
                                   {'name': 'fusion-wear', 'get_url': 'sid=2oq,c1r,p43',
                                    'url': 'http://www.flipkart.com/womens-clothing/fusion-wear/pr?sid=2oq%2Cc1r%2Cp43&filterNone=true'},
                                   {'name': 'women-formal-wear', 'get_url': 'sid=2oq,c1r,f4y',
                                    'url': 'http://www.flipkart.com/womens-clothing/formal-wear/pr?sid=2oq%2Cc1r%2Cf4y&filterNone=true'},
                                   {'name': 'women-jeans-jeggings', 'get_url': 'sid=2oq,c1r,q7g',
                                    'url': 'http://www.flipkart.com/womens-clothing/leggings-jeggings/pr?sid=2oq%2Cc1r%2Cq7g&filterNone=true'},
                                   {'name': 'women-halloween-costumes', 'get_url': 'sid=2oq,c1r,1un',
                                    'url': 'http://www.flipkart.com/womens-clothing/halloween-costumes/pr?sid=2oq%2Cc1r%2C1un&filterNone=true'},
                                   {'name': 'women-winterwear', 'get_url': 'sid=2oq,c1r,67x',
                                    'url': 'http://www.flipkart.com/womens-clothing/winter-seasonal-wear/pr?sid=2oq%2Cc1r%2C67x&filterNone=true'},
                                   {'name': 'women-maternity-wear', 'get_url': 'sid=2oq,c1r,zcy',
                                    'url': 'http://www.flipkart.com/womens-clothing/maternity-wear/pr?sid=2oq%2Cc1r%2Czcy&filterNone=true'},
                                   {'name': 'women-fabrics', 'get_url': 'sid=2oq,c1r,9tg',
                                    'url': 'http://www.flipkart.com/womens-clothing/fabrics/pr?sid=2oq%2Cc1r%2C9tg&filterNone=true'},
                                   {'name': 'women-sports-wear', 'get_url': 'sid=2oq,c1r,6p8',
                                    'url': 'http://www.flipkart.com/womens-clothing/sports-gym-wear/pr?sid=2oq%2Cc1r%2C6p8&filterNone=true'}]

AMAZON_WATCHES_SUB_CAT = [('children-watches',
                           'http://www.amazon.in/s/ref=lp_1350387031_nr_n_2?fst=as%3Aoff&rh=n%3A1350387031%2Cn%3A!1350388031%2Cn%3A2563506031&bbn=1350388031&ie=UTF8&qid=1447914260&rnid=1350388031'),
                          ('women-watches',
                           'http://www.amazon.in/s/ref=lp_1350387031_nr_n_1?fst=as%3Aoff&rh=n%3A1350387031%2Cn%3A!1350388031%2Cn%3A2563505031&bbn=1350388031&ie=UTF8&qid=1447914260&rnid=1350388031'),
                          ('men-watches',
                           'http://www.amazon.in/s/ref=lp_1350387031_nr_n_0?fst=as%3Aoff&rh=n%3A1350387031%2Cn%3A!1350388031%2Cn%3A2563504031&bbn=1350388031&ie=UTF8&qid=1447914260&rnid=1350388031')]

MYNTRA_WATCHES_SUB_CAT = ['men-watches', 'women-watches']

MYNTRA_ACCESSORIES_SUB_CAT = ['men-wallets', 'mens-bags', 'men-belts', 'men-cap-hats', 'jewellery-men', 'women-bags',
                              'men-wallets', 'smart-wearables', 'men-bags-backpacks', 'men-deos-perfumes',
                              'men-bath-body', 'women-bags-wallets', 'women-belts', 'women-jewellery',
                              'men-accessories', 'hair-accessory    ', 'fashion-scarves-stoles-2015',
                              'women-personal-care', 'women-accessories', 'women-skincare', 'women-bodycare',
                              'women-deos-perfumes', 'women-beaty-gift-set']

INDIAN_ROOTS_ACCESSORIES_SUB_CAT = [('women-jewellery', 'http://www.indianroots.in/accessories/jewellery'),
                                    ('women-bags-wallets', 'http://www.indianroots.in/accessories/bags'),
                                    ('shawls', 'http://www.indianroots.in/accessories/wraps/shawls'),
                                    ('stoles-scarves', 'http://www.indianroots.in/accessories/wraps/stoles-scarves'),
                                    ('women-dupattas', 'http://www.indianroots.in/accessories/wraps/dupattas')]

AMAZON_ACCESSORIES_SUB_CAT = [('women-bags-wallets',
                               'http://www.amazon.in/Handbags-Clutches/b/ref=accessories_hp_browb_hbc?ie=UTF8&node=1983338031&pf_rd_m=A1VBAL9TL5WCBF&pf_rd_s=merchandised-search-leftnav&pf_rd_r=0230M6NHH384S4RVWPN2&pf_rd_t=101&pf_rd_p=485867387&pf_rd_i=4516623031'),
                              ('women-jewellery',
                               'http://www.amazon.in/s/ref=lp_5210079031_nr_p_n_target_audience__2?fst=as%3Aoff&rh=n%3A1951048031%2Cn%3A!2152573031%2Cn%3A!2152575031%2Cn%3A5210079031%2Cp_n_target_audience_browse-bin%3A2160297031&bbn=5210079031&ie=UTF8&qid=1447914564&rnid=2160293031'),
                              ('jewellery-men',
                               'http://www.amazon.in/Men-Jewellery/b/ref=j_brb_men?ie=UTF8&node=3543376031&pf_rd_m=A1VBAL9TL5WCBF&pf_rd_s=merchandised-search-leftnav&pf_rd_r=041YKKYRGDHSWEDJKT7S&pf_rd_t=101&pf_rd_p=609344027&pf_rd_i=1951048031')]

FLIPKART_ACCESSORIES_SUB_CAT = [{'name': 'women-accessories', 'get_url': 'sid=2oq,c1r,3gz',
                                 'url': 'http://www.flipkart.com/womens-clothing/accessories/pr?sid=2oq%2Cc1r%2C3gz&filterNone=true'},
                                {'name': 'men-accessories', 'get_url': 'sid=2oq,s9b,8jf',
                                 'url': 'http://www.flipkart.com/mens-clothing/accessories-combo-sets/pr?sid=2oq%2Cs9b%2C8jf&filterNone=true'}]

AMAZON_WOMEN_CLOTHING_SUB_CAT = [('women-swimwear',
                                  'http://www.amazon.in/s/ref=lp_1953602031_nr_n_18?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1953602031%2Cn%3A1968533031&bbn=1953602031&ie=UTF8&qid=1447852523&rnid=1953602031'),
                                 ('women-sports-wear',
                                  'http://www.amazon.in/s/ref=lp_1953602031_nr_n_16?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1953602031%2Cn%3A1968428031&bbn=1953602031&ie=UTF8&qid=1447852523&rnid=1953602031'),
                                 ('women-shorts-skirts',
                                  'http://www.amazon.in/s/ref=lp_1953602031_nr_n_14?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1953602031%2Cn%3A1968511031&bbn=1953602031&ie=UTF8&qid=1447852523&rnid=1953602031'),
                                 ('women-shorts-skirts',
                                  'http://www.amazon.in/s/ref=lp_1953602031_nr_n_13?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1953602031%2Cn%3A1968510031&bbn=1953602031&ie=UTF8&qid=1447852523&rnid=1953602031'),
                                 ('women-tops-tees',
                                  'http://www.amazon.in/s/ref=lp_1953602031_nr_n_12?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1953602031%2Cn%3A1968542031&bbn=1953602031&ie=UTF8&qid=1447852523&rnid=1953602031'),
                                 ('women-pants',
                                  'http://www.amazon.in/s/ref=lp_1953602031_nr_n_11?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1953602031%2Cn%3A1968547031&bbn=1953602031&ie=UTF8&qid=1447852523&rnid=1953602031'),
                                 ('women-shrugs-jackets',
                                  'http://www.amazon.in/s/ref=lp_1953602031_nr_n_10?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1953602031%2Cn%3A1968505031&bbn=1953602031&ie=UTF8&qid=1447852523&rnid=1953602031'),
                                 ('women-nightdress',
                                  'http://www.amazon.in/s/ref=lp_1953602031_nr_n_9?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1953602031%2Cn%3A1968498031&bbn=1953602031&ie=UTF8&qid=1447852523&rnid=1953602031'),
                                 ('women-briefs',
                                  'http://www.amazon.in/s/ref=lp_1953602031_nr_n_8?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1953602031%2Cn%3A1968457031&bbn=1953602031&ie=UTF8&qid=1447852523&rnid=1953602031'),
                                 ('women-jeans-jeggings',
                                  'http://www.amazon.in/s/ref=lp_1953602031_nr_n_7?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1953602031%2Cn%3A1968456031&bbn=1953602031&ie=UTF8&qid=1447852523&rnid=1953602031'),
                                 ('women-shrugs-jackets',
                                  'http://www.amazon.in/s/ref=lp_1953602031_nr_n_6?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1953602031%2Cn%3A1968449031&bbn=1953602031&ie=UTF8&qid=1447852523&rnid=1953602031'),
                                 ('dresses-jumpsuits',
                                  'http://www.amazon.in/s/ref=lp_1953602031_nr_n_5?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1953602031%2Cn%3A1968448031&bbn=1953602031&ie=UTF8&qid=1447852523&rnid=1953602031'),
                                 ('women-jeans-jeggings',
                                  'http://www.amazon.in/s/ref=lp_1953602031_nr_n_4?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1953602031%2Cn%3A1968447031&bbn=1953602031&ie=UTF8&qid=1447852523&rnid=1953602031'),
                                 ('women-sweaters-sweatshirts',
                                  'http://www.amazon.in/s/ref=lp_1953602031_nr_n_3?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1953602031%2Cn%3A1968446031&bbn=1953602031&ie=UTF8&qid=1447852523&rnid=1953602031'),
                                 ('women-ethnic-wear',
                                  'http://www.amazon.in/s/ref=lp_1953602031_nr_n_2?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1953602031%2Cn%3A1968253031&bbn=1953602031&ie=UTF8&qid=1447852523&rnid=1953602031'),
                                 ('women-dress-material',
                                  'http://www.amazon.in/s/ref=lp_1953602031_nr_n_1?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1953602031%2Cn%3A1968445031&bbn=1953602031&ie=UTF8&qid=1447852523&rnid=1953602031')]

PERNIA_WOMEN_CLOTHING_SUB_CAT = [('women-blazers', 'blazer'), 'anarkali', 'blouse',
                                 ('women-kurtas-kutis-suits', 'body-suit'),
                                 ('women-capes', 'cape'), ('women-dress-material', 'dresses'),
                                 ('women-dupatta', 'dupattas'), ('women-gown', 'gown'),
                                 ('women-shrugs-jackets', 'jackets'), ('dresses-jumpsuits', 'jumpsuit'), 'kaftan',
                                 ('women-kurtas-kutis-suits', 'kurta-sets-salwar-kameez'), ('lehenga-choli', 'lehenga'),
                                 ('women-nightdress', 'nightwear'), ('women-over-coat', 'over-coat'),
                                 ('women-pants', 'pants'), 'petticoat', ('sarees', 'sari'), ('women-shirts', 'shirt'),
                                 ('women-shorts-skirts', 'shorts'), ('women-shrugs-jackets', 'shrugs'),
                                 ('women-shorts-skirts', 'skirts'), ('women-swimwear', 'swimsuits'),
                                 ('women-tops-tees', 't-shirt'), ('women-tops-tees', 'top'), ('tunics', 'tunic'),
                                 ('women-waistcoat', 'waistcoat')]

PERNIA_ACCESSORIES_SUB_CAT = [('fashion-scarves-stoles-2015', 'stole-scarves', 'clothing'),
                              ('women-bags-wallets', 'bags'), ('women-bags-wallets', 'belts'),
                              ('women-jewellery', 'fine-jewellery'), ('women-jewellery', 'jewellery'),
                              ('hair-accessory', 'hair-accessories'), 'rakhi', 'shawls', 'sunglasses']

amazonapi_config = {
    'access_key': '',
    'secret_key': '',
    'associate_tag': 'pykart-21',
    'locale': 'in'
}

FLIPKART_PRODUCT_LISTING_URL = 'http://www.flipkart.com/lc/pr/pv1/spotList1/spot1/productList'
FLIPKART_MEN_CLOTHING_SUB_CAT = [{'name': 'men-tshirts', 'get_url': 'sid=2oq,s9b,j9y',
                                  'url': 'http://www.flipkart.com/mens-clothing/t-shirts/pr?sid=2oq%2Cs9b%2Cj9y&filterNone=true'},
                                 {'name': 'men-formal-shirts', 'get_url': 'sid=2oq,s9b,mg4,fh5',
                                  'url': 'http://www.flipkart.com/mens-clothing/shirts/formal-shirts/pr?sid=2oq%2Cs9b%2Cmg4%2Cfh5&otracker=hp_nmenu_sub_men_0_Formal%20Shirts'},
                                 {'name': 'men-casual-shirts', 'get_url': 'sid=2oq,s9b,mg4,vg6',
                                  'url': 'http://www.flipkart.com/mens-clothing/shirts/casual-party-wear-shirts/pr?sid=2oq%2Cs9b%2Cmg4%2Cvg6&filterNone=true'},
                                 {'name': 'men-jeans', 'get_url': 'sid=2oq,s9b,94h',
                                  'url': 'http://www.flipkart.com/mens-clothing/jeans/pr?sid=2oq%2Cs9b%2C94h&filterNone=true'},
                                 {'name': 'men-shorts', 'get_url': 'sid=2oq,s9b,vde',
                                  'url': 'http://www.flipkart.com/mens-clothing/sports-wear/shorts/pr?sid=2oq%2Cs9b%2C6gr%2C59q&filterNone=true'},
                                 {'name': 'men-track-pants', 'get_url': 'sid=2oq,s9b,6gr,rfn',
                                  'url': 'http://www.flipkart.com/mens-clothing/sports-wear/track-pants/pr?sid=2oq%2Cs9b%2C6gr%2Crfn&filterNone=true'},
                                 {'name': 'men-sports-tshirt', 'get_url': 'sid=2oq,s9b,6gr,j9y',
                                  'url': 'http://www.flipkart.com/mens-clothing/sports-wear/t-shirts/pr?sid=2oq%2Cs9b%2C6gr%2Cj9y&filterNone=true'},
                                 {'name': 'men-sweatshirts', 'get_url': 'sid=2oq,s9b,qgu,8vm',
                                  'url': 'http://www.flipkart.com/mens-clothing/winter-seasonal-wear/sweaters/pr?sid=2oq%2Cs9b%2Cqgu%2Cl8m&filterNone=true'},
                                 {'name': 'men-jacket-blazers', 'get_url': 'sid=2oq,s9b,qgu,8cd',
                                  'url': 'http://www.flipkart.com/mens-clothing/winter-seasonal-wear/jackets/pr?sid=2oq%2Cs9b%2Cqgu%2C8cd&filterNone=true'},
                                 {'name': 'men-sweaters', 'get_url': 'sid=2oq,s9b,qgu,8cd',
                                  'url': 'http://www.flipkart.com/mens-clothing/winter-seasonal-wear/sweaters/pr?sid=2oq%2Cs9b%2Cqgu%2Cl8m&filterNone=true'},
                                 {'name': 'men-raincoats', 'get_url': 'sid=2oq,s9b,qgu,hf8',
                                  'url': 'http://www.flipkart.com/mens-clothing/winter-seasonal-wear/raincoats/pr?sid=2oq%2Cs9b%2Cqgu%2Chf8&filterNone=true'},
                                 {'name': 'men-shirts', 'get_url': 'sid=2oq,s9b,mg4',
                                  'url': 'http://www.flipkart.com/mens-clothing/shirts/pr?sid=2oq%2Cs9b%2Cmg4&filterNone=true'},
                                 {'name': 'men-sports-wear', 'get_url': 'sid=2oq,s9b,6gr',
                                  'url': 'http://www.flipkart.com/mens-clothing/sports-wear/pr?sid=2oq%2Cs9b%2C6gr&filterNone=true'},
                                 {'name': 'men-innerwear-sleepwear', 'get_url': 'sid=2oq,s9b,b1a',
                                  'url': 'http://www.flipkart.com/mens-clothing/inner-wear-sleep-wear/pr?sid=2oq%2Cs9b%2Cb1a&filterNone=true'},
                                 {'name': 'men-casual-trousers', 'get_url': 'sid=2oq,s9b,9uj',
                                  'url': 'http://www.flipkart.com/mens-clothing/trousers/pr?sid=2oq%2Cs9b%2C9uj&filterNone=true'},
                                 {'name': 'men-suits', 'get_url': 'sid=2oq,s9b,q9f',
                                  'url': 'http://www.flipkart.com/mens-clothing/suits-blazers/pr?sid=2oq%2Cs9b%2Cq9f&filterNone=true'},
                                 {'name': 'men-ethnic-wear', 'get_url': 'sid=2oq,s9b,3a0',
                                  'url': 'http://www.flipkart.com/mens-clothing/ethnic-wear/pr?sid=2oq%2Cs9b%2C3a0&filterNone=true'},
                                 {'name': 'men-fabrics', 'get_url': 'sid=2oq,s9b,9hz',
                                  'url': 'http://www.flipkart.com/mens-clothing/fabrics/pr?sid=2oq%2Cs9b%2C9hz&filterNone=true'},
                                 {'name': 'men-halloween-costumes', 'get_url': 'sid=2oq,s9b,m9a',
                                  'url': 'http://www.flipkart.com/mens-clothing/halloween-costumes/pr?sid=2oq%2Cs9b%2Cm9a&filterNone=true'}]

FLIPKART_WATCHES_SUB_CAT = [
    {'name': 'men-watches', 'get_url': 'sid=r18&p[]=facets.ideal_for%5B%5D=Men&p[]=facets.ideal_for%5B%5D=Boys',
     'url': 'http://www.flipkart.com/watches/pr?p[]=facets.ideal_for%255B%255D%3DMen&p[]=facets.ideal_for%255B%255D%3DBoys&p[]=sort%3Dpopularity&sid=r18&filterNone=true'},
    {'name': 'women-watches', 'get_url': 'sid=r18&p[]=facets.ideal_for%5B%5D=Women&p[]=facets.ideal_for%5B%5D=Girls',
     'url': 'http://www.flipkart.com/watches/pr?p[]=facets.ideal_for%255B%255D%3DWomen&p[]=facets.ideal_for%255B%255D%3DGirls&p[]=sort%3Dpopularity&sid=r18&filterNone=true'},
    {'name': 'couple-watches', 'get_url': 'sid=r18&p[]=facets.ideal_for%5B%5D=Couple',
     'url': 'http://www.flipkart.com/watches/pr?p[]=facets.ideal_for%255B%255D%3DCouple&p[]=sort%3Dpopularity&sid=r18&filterNone=true'}]

PERNIA_BASE_URL = 'http://www.perniaspopupshop.com'

AMAZON_BASE_URL = 'http://www.amazon.in'

AMAZON_MEN_CLOTHING_SUB_CAT = [('men-jeans',
                                'http://www.amazon.in/Mens-Jeans/b/ref=amb_link_191150247_10?ie=UTF8&node=1968076031&pf_rd_m=A1VBAL9TL5WCBF&pf_rd_s=merchandised-search-leftnav&pf_rd_r=18GR9J2C50D2FNJPBMBZ&pf_rd_t=101&pf_rd_p=757016607&pf_rd_i=1968024031'),
                               ('men-tshirts',
                                'http://www.amazon.in/Mens-Tshirts/b/ref=amb_link_191150247_11?ie=UTF8&node=1968120031&pf_rd_m=A1VBAL9TL5WCBF&pf_rd_s=merchandised-search-leftnav&pf_rd_r=1Z8F2CKWZ84J8Y4V1V1Z&pf_rd_t=101&pf_rd_p=757016607&pf_rd_i=1968024031'),
                               ('men-casual-shirts',
                                'http://www.amazon.in/s/ref=lp_1968093031_nr_n_0?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1968024031%2Cn%3A1968093031%2Cn%3A1968094031&bbn=1968093031&ie=UTF8&qid=1447850334&rnid=1968093031'),
                               ('men-dress-shirts',
                                'http://www.amazon.in/s/ref=lp_1968093031_nr_n_1?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1968024031%2Cn%3A1968093031%2Cn%3A1968095031&bbn=1968093031&ie=UTF8&qid=1447850334&rnid=1968093031'),
                               ('men-formal-shirts',
                                'http://www.amazon.in/s/ref=lp_1968093031_nr_n_2?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1968024031%2Cn%3A1968093031%2Cn%3A1968096031&bbn=1968093031&ie=UTF8&qid=1447850334&rnid=1968093031'),
                               ('men-innerwear-sleepwear',
                                'http://www.amazon.in/s/ref=lp_1968126031_nr_n_0?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1968024031%2Cn%3A1968126031%2Cn%3A1968127031&bbn=1968126031&ie=UTF8&qid=1447850956&rnid=1968126031'),
                               ('men-innerwear-sleepwear',
                                'http://www.amazon.in/s/ref=lp_1968126031_nr_n_1?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1968024031%2Cn%3A1968126031%2Cn%3A1968128031&bbn=1968126031&ie=UTF8&qid=1447851035&rnid=1968126031'),
                               ('men-innerwear-sleepwear',
                                'http://www.amazon.in/s/ref=lp_1968126031_nr_n_2?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1968024031%2Cn%3A1968126031%2Cn%3A1968134031&bbn=1968126031&ie=UTF8&qid=1447851035&rnid=1968126031'),
                               ('men-innerwear-sleepwear',
                                'http://www.amazon.in/s/ref=lp_1968126031_nr_n_3?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1968024031%2Cn%3A1968126031%2Cn%3A1968135031&bbn=1968126031&ie=UTF8&qid=1447851035&rnid=1968126031'),
                               ('men-innerwear-sleepwear',
                                'http://www.amazon.in/Mens-Sleep-Lounge-Wear/b/ref=amb_link_191150247_14?ie=UTF8&node=1968082031&pf_rd_m=A1VBAL9TL5WCBF&pf_rd_s=merchandised-search-leftnav&pf_rd_r=19YSJPY7A0VNAG64A9NM&pf_rd_t=101&pf_rd_p=757016607&pf_rd_i=1968024031')]

MYNTRA_MEN_FOOTWARE_SUB_CAT = ['men-casual-shoes', 'men-sports-shoes', 'men-formal-shoes', 'men-sandals',
                               'men-flip-flops', 'men-socks', 'men-formal-footware']

AMAZON_MEN_FOOTWARE_SUB_CAT = [('men-socks',
                                'http://www.amazon.in/s/ref=lp_1968024031_nr_n_9?fst=as%3Aoff&rh=n%3A1571271031%2Cn%3A!1571272031%2Cn%3A1968024031%2Cn%3A1968103031&bbn=1968024031&ie=UTF8&qid=1447914816&rnid=1968024031'),
                               ('men-flip-flops',
                                'http://www.amazon.in/Flip-Flop-Slippers/b/ref=SH_BB_Cat_M_FF?ie=UTF8&node=1983575031&pf_rd_m=A1VBAL9TL5WCBF&pf_rd_s=merchandised-search-leftnav&pf_rd_r=0R074V4KT4Y2CVR1BGCA&pf_rd_t=101&pf_rd_p=685526907&pf_rd_i=1983396031'),
                               ('men-sandals',
                                'http://www.amazon.in/Sandals-Floaters/b/ref=SH_BB_Cat_M_SNDL?ie=UTF8&node=1983571031&pf_rd_m=A1VBAL9TL5WCBF&pf_rd_s=merchandised-search-leftnav&pf_rd_r=0R074V4KT4Y2CVR1BGCA&pf_rd_t=101&pf_rd_p=685526907&pf_rd_i=1983396031'),
                               ('men-casual-shoes',
                                'http://www.amazon.in/Sneakers-Men/b/ref=SH_BB_Cat_M_SNKR?ie=UTF8&node=1983577031&pf_rd_m=A1VBAL9TL5WCBF&pf_rd_s=merchandised-search-leftnav&pf_rd_r=0R074V4KT4Y2CVR1BGCA&pf_rd_t=101&pf_rd_p=685526907&pf_rd_i=1983396031'),
                               ('men-formal-shoes',
                                'http://www.amazon.in/Men-Formal-Shoes/b/ref=SH_BB_Cat_M_formals?ie=UTF8&node=1983572031&pf_rd_m=A1VBAL9TL5WCBF&pf_rd_s=merchandised-search-leftnav&pf_rd_r=0R074V4KT4Y2CVR1BGCA&pf_rd_t=101&pf_rd_p=685526907&pf_rd_i=1983396031'),
                               ('men-sports-shoes',
                                'http://www.amazon.in/Sports-Outdoor-Men-Shoes/b/ref=SH_BB_Cat_M_sports?ie=UTF8&node=1983519031&pf_rd_m=A1VBAL9TL5WCBF&pf_rd_s=merchandised-search-leftnav&pf_rd_r=0R074V4KT4Y2CVR1BGCA&pf_rd_t=101&pf_rd_p=685526907&pf_rd_i=1983396031')]

INDIAN_ROOTS_BASE_URL = 'http://www.indianroots.in'

MYNTRA_WOMEN_CLOTHING_SUB_CAT = ['women-kurtas-kutis-suits', 'sarees', 'fusion-wear', 'indian-bottomwear-fashion-2015',
                                 'lehenga-choli', 'women-dress-material', 'women-dupatta', 'women-tops-tees',
                                 'dresses-jumpsuits', 'women-jeans-jeggings', 'women-shorts-skirts',
                                 'women-sports-tshirt-collection', 'tunics', 'women-sweaters-sweatshirts',
                                 'women-shirts', 'women-trousers', 'women-shrugs-jackets',
                                 'women-trackpants-tracksuits', 'bra', 'women-briefs', 'women-nightdress',
                                 'women-swimwear', 'women-shapewear']

INDIAN_ROOTS_WOMEN_CLOTHING_SUB_CAT = [('sarees', 'http://www.indianroots.in/women/saree'),
                                       ('women-kurtas-kutis-suits', 'http://www.indianroots.in/women/kurti'),
                                       ('women-kurtas-kutis-suits', 'http://www.indianroots.in/women/salwar-kameez'),
                                       ('anarkali', 'http://www.indianroots.in/women/salwar-kameez/anarkalis'),
                                       ('lehenga-choli', 'http://www.indianroots.in/women/lehenga'),
                                       ('fusion-wear', 'http://www.indianroots.in/women/fusion-wear'),
                                       ('burkas', 'http://www.indianroots.in/women/burkas')]

MYNTRA_MEN_CLOTHING_SUB_CAT = ['men-track-pants', 'men-kurtas', 'men-casual-shirts',
                               'men-casual-trousers', 'men-innerwear-sleepwear',
                               'men-jeans', 'men-tshirts', 'men-jacket-blazers', 'men-formal-shirts',
                               'men-suits', 'men-sports-tshirt', 'men-formal-trousers', 'men-shorts', 'sweatshirts',
                               'boys-t-shirt', 'boys-shirt', 'boys-winter-wear']

INDIAN_ROOTS_MEN_CLOTHING_SUB_CAT = [('men-jacket-blazers', 'http://www.indianroots.in/men/jackets'),
                                     ('men-kurtas', 'http://www.indianroots.in/men/kurta-pyjama'),
                                     ('men-designer-cloths', 'http://www.indianroots.in/men/sherwani'),
                                     ('men-formal-shirts', 'http://www.indianroots.in/men/shirts/formal'),
                                     ('men-casual-shirts', 'http://www.indianroots.in/men/shirts/casual'),
                                     ('men-tshirts',
                                      'http://www.indianroots.in/men/t-shirts-vests-underwear-socks-watches'),
                                     ('men-track-pants', 'http://www.indianroots.in/men/dhoti-pants')]

INDIAN_ROOTS_WOMEN_FOOTWARE_SUB_CAT = [('women-designer-shoes', 'http://www.indianroots.in/accessories/footwear')]

MYNTRA_SUB_CATEGORY_LIST = MYNTRA_MEN_CLOTHING_SUB_CAT + MYNTRA_WOMEN_CLOTHING_SUB_CAT

UTSAVFASHION_BASE_URL = 'http://www.utsavfashion.in/store/generalloadplpsearch.aspx'

UTSAVFASHION_WOMEN_CLOTHING_SUB_CAT = [
    ('sarees', 'http://www.utsavfashion.in/store/generalloadplpsearch.aspx?cat=saree'),
    ('women-kurtas-kutis-suits', 'http://www.utsavfashion.in/store/generalloadplpsearch.aspx?cat=salwar'),
    ('lehenga-choli', 'http://www.utsavfashion.in/store/generalloadplpsearch.aspx?cat=lehenga'),
    ('women-kurtas-kutis-suits', 'http://www.utsavfashion.in/store/generalloadplpsearch.aspx?cat=indowestern'),
    ('women-ethnic-wear', 'http://www.utsavfashion.in/store/generalloadplpsearch.aspx?cat=essentials'),
    ('burkas', 'http://www.utsavfashion.in/store/generalloadplpsearch.aspx?q=cid:17&gender=women'),
    ('women-winterwear', 'http://www.utsavfashion.in/store/generalloadplpsearch.aspx?q=cid:15&gender=women')]

UTSAVFASHION_MEN_CLOTHING_SUB_CAT = [
    ('men-kurtas', 'http://www.utsavfashion.in/store/generalloadplpsearch.aspx?q=cid:7&type=kurta pajama'),
    ('men-designer-cloths', 'http://www.utsavfashion.in/store/generalloadplpsearch.aspx?q=cid:7&type=sherwani'),
    ('men-designer-cloths',
     'http://www.utsavfashion.in/store/generalloadplpsearch.aspx?q=cid:7&type=indowestern dress'),
    ('men-designer-cloths',
     'http://www.utsavfashion.in/store/generalloadplpsearch.aspx?q=cid:7&type=mens shirts'),
    ('men-kurtas',
     'http://www.utsavfashion.in/store/generalloadplpsearch.aspx?q=cid:7&type=men kurta'),
    ('men-designer-cloths',
     'http://www.utsavfashion.in/store/generalloadplpsearch.aspx?q=cid:7&type=dhoti sherwani'),
    ('men-winterwear', 'http://www.utsavfashion.in/store/generalloadplpsearch.aspx?q=cid:15&gender=men')]

UTSAVFASHION_MEN_FOOTWARE_SUB_CAT = [
    ('men-designer-footware', 'http://www.utsavfashion.in/store/generalloadplpsearch.aspx?q=cid:10&gender=men')]

UTSAVFASHION_WOMEN_FOOTWARE_SUB_CAT = [
    ('women-designer-footware', 'http://www.utsavfashion.in/store/generalloadplpsearch.aspx?q=cid:10&gender=women')]

UTSAVFASHION_ACCESSORIES_SUB_CAT = [
    ('women-jewellery', 'http://www.utsavfashion.in/store/generalloadplpsearch.aspx?q=cid:5&cat=jewelry'),
    ('women-bags-wallets', 'http://www.utsavfashion.in/store/generalloadplpsearch.aspx?q=cid:13&cat=handbags')]
