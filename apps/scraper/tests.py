# Create your tests here.
import csv

import requests
from bs4 import BeautifulSoup


def scraper_exc_designers():
    r = requests.get('http://www.exclusively.com/designers-list')
    soup = BeautifulSoup(r.content)
    div = soup.find('div', {'class': 'dnbDetails'})
    items = div.find_all('li', {'itemprop': 'itemListElement'})

    with open('exc_designers.csv', 'wb') as f:
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        wr.writerow(['S.Np.', 'Designers'])
        i = 1
        for item in items:

            extra_li = item.find_all('li')
            if extra_li:
                for li in extra_li:
                    wr.writerow([i, li.text.encode('utf-8').strip()])
                    i += 1
            else:
                wr.writerow([i, item.text.encode('utf-8').strip()])
                i += 1


if __name__ == '__main__':
    scraper_exc_designers()
