from celery.task.base import task
from django.db import connection

from .scrapers import MyntraScraper, AmazonScraper, FlipkartScraper, PerniaScraper, IndianRootsScraper, \
    UtsavFashionScraper


@task
def run_pernia_scraper():
    pernia_scraper = PerniaScraper()
    pernia_scraper.start()
    refresh_mat_views()


@task
def run_myntra_scraper():
    myntra_scraper = MyntraScraper()
    myntra_scraper.start()

    refresh_mat_views.delay()


@task
def run_amazon_scraper():
    amazon_scraper = AmazonScraper()
    amazon_scraper.start()

    #refresh_mat_views()


@task
def run_flipkart_scraper():
    flipkart_scraper = FlipkartScraper()
    flipkart_scraper.start()

    #refresh_mat_views()


@task
def run_indian_roots_scraper():
    ir_scraper = IndianRootsScraper()
    ir_scraper.start()

    refresh_mat_views()


@task
def run_utsavfashion_scraper():
    uf_scraper = UtsavFashionScraper()
    uf_scraper.start()

    refresh_mat_views()


@task
def refresh_mat_views():
    sql = '''refresh materialized view brand_level_dataset;
             refresh materialized view sub_cat_level_dataset;
             refresh materialized view competitor_level_dataset;'''

    cursor = connection.cursor()
    cursor.execute(sql)
