import csv
import re

from bs4 import BeautifulSoup
import requests


class BaseSraper(object):
    request_header = {"content-type": "application/json"}

    def send_request(self, url, json=True):
        r = requests.get(url, headers=self.request_header)

        if r.status_code == 200:
            if json == True:
                return r.json()
            else:
                return r.content
        else:
            raise Exception

    @staticmethod
    def extract_numbers(string, encode=True):
        if encode:
            return ''.join(re.findall(r'\d+', string.encode('utf-8')))
        else:
            return ''.join(re.findall(r'\d+', string))


class ZopposScraper(BaseSraper):
    base_url = 'http://www.zappos.com'
    categories = ['/mens']

    def start(self):
        print('Zappos Scraper Started ....\n')
        with open('zappos.csv', 'wb') as f:
            wr = csv.writer(f, quoting=csv.QUOTE_ALL)
            wr.writerow(['Category', 'Sub-Category', 'Filter Type', 'Filters', 'POGs'])
            for cat in self.categories:
                print ('Fetching Category {}\n'.format(cat))
                url = self.base_url + cat
                print ('Sending Request to - {}\n'.format(url))
                r = requests.get(url)
                soup = BeautifulSoup(r.content)

                sidebar = soup.find('div', {'id': 'edSideCol'})
                catnav_divs = sidebar.find_all('div', {'class': 'catNav '})
                for cat_nav in catnav_divs:
                    a_sub_cats = cat_nav.find_all('a')
                    category = a_sub_cats[0].text.encode('utf-8')
                    for cat in a_sub_cats[1:]:
                        sub_category = cat.text.encode('utf-8')
                        url = self.base_url + cat.get('href')
                        print ('Sending Request to - {}\n'.format(url))
                        r = requests.get(url)
                        soup = BeautifulSoup(r.content)
                        navbar = soup.find('div', {'id': 'naviCenter'})
                        if navbar:
                            h4s = navbar.find_all('h4', {'class': 'stripeOuter'})
                            divs = navbar.find_all('div')

                            for i in xrange(len(h4s)):
                                filter_category = h4s[i].text.encode('utf-8').strip()

                                filters = divs[i].find_all('a')
                                for a in filters:
                                    try:
                                        a_title = a.get('title').split('-')
                                        filter = a_title[0].encode('utf-8').strip()
                                        pogs = self.extract_numbers(a_title[1])
                                        wr.writerow([category, sub_category, filter_category, filter, pogs])
                                    except Exception as e:
                                        print e
        print('\nDone...')


if __name__ == '__main__':
    zoppos = ZopposScraper()
    zoppos.start()
